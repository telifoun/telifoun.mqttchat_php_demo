<?php     /*
     * Copyright (C) 2019 Gaddour, Gaddour Mohamed
     * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
     * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
     * Any misuse of product or income related to its exploitation is strictly prohibited.
     */
    $currentversion = "1.0";

    /** MQTTCHAT GENRAL PARAMS **/
    /**
     * Your website domain name : Example: exemple.com (without "www")
     */
    define("DOMAIN","");

    /**
     * Base URL of your site followed by MQTTCHAT directory : Example: https://www.exemple.com/mqttchat
     */
    define("BASE_URL","http://localhost/telifoun.mqttchat_php_demo/mqttchat");

    /**
     * MQTT Server IP Address : Do not change
     */
    define("MQTT_BROKER","cluster1.telifoun.com");

    /**
     * WEBSOCKET port used for connection : Do not change 
     * [two options : secure or not secure websockets]
     */

    /* Use secure websocket for websites with https (ssl certificate web)**/
    define("MQTT_PORT",1885);
    define("USE_SSL",1);


    /* Use standard websocket for websites with http (no ssl certificate web)**/
    /*define("MQTT_PORT",1882);
    define("USE_SSL",0);*/

    /**
     * Choose auto value if you want MQTTCHAT to automatically detect the language by checking if there is a LANG variable associated with a cookie $_COOKIE ["lang"] or to a session $_ESSION ["lang"] .
     * You can force MQTTCHAT to use a specific language en, fr, ar etc ..
     */
    define("LANG", "auto"); 

    /**
     * Option for websites that use friends concept.
     */
    define("USE_FRIENDS",0);  // use freinds list

    /**
     * Indicates whether to include or not jQuery.js in MQTTCHAT javascrit code js.php
     */
    define("INCLUDE_JQUERY",1); 

    /**
     * Indicates whether to include or not jQuery.tmpl.js (jQuery templates) in MQTTCHAT javascript code js.php
     */
    define("INCLUDE_JQUERY_TMPL",1);
    /**
     * Indicates whether to save or not logs in MQTTCHAT/log/log.txt
     */
    define("SAVE_LOGS",1);

    /**
     * Show or not PHP Exceptions and notices
     */
    define("DEV_MODE",1);
    
    define("ERROR_LOGGING",1);  

    /**
     * User can edit profile photo and profile informations from 
     * MQTTCHAT or NOT
     */
    define("EDIT_PROFILE", 1);  // edit profile from MQTTCHAT

    /**
     * The number of characters of userid var : Do not change
     */
    define ("USERID_LENGTH",12);

    /**
     * The height in pixels of textarea field used to send messages
     */
    define("TEXTAREA_HEIGHT",25);

    /**
     * The maximum number of characters to be entered in a message
     */
    define("TEXTAREA_MAXLENGTH",250); // max 500

    /**
     * Time in ms of inactivity to go from online status to away status.
     */
    define ("IDLE_TIMEOUT",200000); 

    /**
     * A PHP array that contains the list of modules you want to load into MQTTCHAT.
     * it can be empty
     */
    $modules =array("visio");

    /**
     * A PHP array that contains the list of plugins you want to load into MQTTCHAT.
     * It must contain at least the smileys plugin that is required to send and receive text messages.
     */
    $plugins = array(0=>"smileys",1=>"stickers",2=>"pictures_disk",3=>"records",4=>"pictures_cam",5=>"visio_msg");
    /**
     * Specifies the width in pixels of the thumbnail that will be created from the 
     * photo sent by the user
     */
    define("THUMBNAIL_WIDTH",250);

    /** AJAX DEFAULT SETIINGS **/ 
    define ("AJAX_TIMEOUT","20000");
    define ("AJAX_DATA_TYPE","JSON");
    define ("AJAX_DATA_METHOD","GET");
    define ("AJAX_REQUEST_OFFSET",20);