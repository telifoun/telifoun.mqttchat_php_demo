MQTTCHAT Self Hosted Installation guide

1 - Register to mqttchat and add your domain name to your customer area. 
then Go to the settings section of your customer area. Click on download now link in the "download link" panel to download mqttchat source code.
You have to extract the downloaded zip file in the root folder of your website "/mqttchat".


2 - Add the oauth2.O authentication settings to your SDK config from customer area "Authentication settings" Panel.
You have to add CLIENT_ID and CLIENT_SECRET settings to SDK config file located at "mqttchat/sdk/src/telifoun/mqttchat/config.php".

3 - Add necessary config to MQTTCHAT application from your customer area "Application settings" Panel.
You have to edit DOMAIN and BASE_URL settings in mqttchat config file located at "mqttchat/config.php" . 

Be careful ! 
If you already use jquery in your website and to avoid a conflict by adding jQuery twice, you have to tell mqttchat not to include jQuery by setting INCLUDE_JQUERY to 0.

4 - Once the application is installed and working properly. 
you have to change the value of dev_mode to 0 in the file "mqttchat/config.php" to activate the cache mechanism and the application gets faster.

That is all. 