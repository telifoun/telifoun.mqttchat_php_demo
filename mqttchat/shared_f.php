<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * 
 * @param type $data
 * @return type
 */
function ok($data){
 return array("ok"=>true,"response"=>$data);   
}
    
/**
 * 
 * @param type $code
 * @param type $error
 * @return type
 */
function ko($code,$error){
 return array("ok"=>false,"response"=>array("code"=>$code,"error"=>$error));     
}

/**
 * 
 * @param type $value
 * @return type
 */
function stripSlashesDeep($value) {
 $value = is_array($value) ? array_map('stripSlashesDeep', $value) : stripslashes($value);
 return $value;
}

/**
 * 
 * @param type $url
 * @return type
 */
function parse_photo_in_url($url){
 $matches = array();
 preg_match('/[0-9]*_[0-9]*.(jpg|png|jpeg)/i', $url, $matches); 
 return (isset($matches[0])?$matches[0]:null); 
}