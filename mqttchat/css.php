<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

$mtime = explode(" ",microtime());
$starttime = $mtime[1]+$mtime[0];
$HTTP_USER_AGENT = '';  
$useragent = (isset($_SERVER["HTTP_USER_AGENT"])) ? $_SERVER["HTTP_USER_AGENT"] : $HTTP_USER_AGENT;

include_once dirname(__FILE__) .DIRECTORY_SEPARATOR.'config.php';
include_once dirname(__FILE__) .DIRECTORY_SEPARATOR.'mqttchat.php';

ob_start();


if(file_exists(dirname(__FILE__) .DIRECTORY_SEPARATOR."cache".DIRECTORY_SEPARATOR."mqttchat.$lang.css") && (DEV_MODE!=1)){
   if(!empty($_SERVER['HTTP_IF_MODIFIED_SINCE'])&&strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE'])==filemtime(dirname(__FILE__).DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR."mqttchat.$lang.css")){
	   header("HTTP/1.1 304 Not Modified");
	   exit();
	}
  	readfile(dirname(__FILE__).DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR."mqttchat.$lang.css");
	$css = ob_get_clean();
    
} else{


   if(isset($modules) && sizeof($modules)>0){
    foreach($modules as $module){  
    if(file_exists(dirname(__FILE__). '/modules/'.$module.'/themes/main.css')){
    include_once dirname(__FILE__).  '/modules/'.$module.'/themes/main.css';
    }
    }
  }
   
    $css = minify(ob_get_clean());
    
    $fp = @fopen(dirname(__FILE__).DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR."mqttchat.$lang.css",'w');
    @fwrite($fp,$css);
    @fclose($fp);
    
 }

 $lastModified = filemtime(dirname(__FILE__).DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR."mqttchat.$lang.css");    
    
header('Content-type: text/css;charset=utf-8');
header("Last-Modified: ".gmdate("D, d M Y H:i:s",$lastModified )." GMT");
header('Expires: '.gmdate("D, d M Y H:i:s",time()+3600*24*365).' GMT');

echo minify($css);

$mtime = explode(" ",microtime());
$endtime = $mtime[1]+$mtime[0];

echo "\n\n/* Execution time: ".($endtime-$starttime)." seconds */";
function cleanInput($input){
	$input = preg_replace("/[^+A-Za-z0-9\_]/","",trim($input));
	return strtolower($input);
}

function minify($css){
	$css = preg_replace('#\s+#',' ',$css);
	$css = preg_replace('#/\*.*?\*/#s','',$css);
	$css = str_replace('; ',';',$css);
	$css = str_replace(': ',':',$css);
	$css = str_replace(' {','{',$css);
	$css = str_replace('{ ','{',$css);
	$css = str_replace(', ',',',$css);
	$css = str_replace('} ','}',$css);
	$css = str_replace(';}','}',$css);
	return trim($css);
}