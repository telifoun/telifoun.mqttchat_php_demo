<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once dirname(__FILE__) .'/../../../../config.php';
$base_url=BASE_URL;
echo <<<EOD
    <div id="mqttchat-header">        

        <div id="mqttchat-header-left-part"> 
        </div>        
        
         <div id="mqttchat-header-right-part">    
          <div id="mqttchat-settings">
            <div id="settings_btn"></div>            
          </div>  
        </div>
                
    </div>            
    
    <div id="mqttchat-main-container">        
        <div id="mqttchat_leftbar"></div>        
        <div id="mqttchat_rightbar"></div>        
    </div>    

    <div id="mqttchat_ext_panel" class="mqttchat_ext_windows"> 
        <div id="mqttchat_windows_titlebar">  
            <div id="mqttchat-windows-title"><span>test-title</span></div>  
            <div id="mqttchat-windows-close"><img src="${base_url}/images/removewhite.svg"/></div>  
       </div>
       <div class="mqttchat_ext_windows_content">
       <div class="mqttchat-content-loading"></div>  
       <div id="mqttchat_ext_content"></div>
       </div>
    </div>

   <div id="mqttchat-visio-element" style="display:none;"></div>  
   <div id="mqttchat-disconnect-element" style="display:none;"></div>  
EOD;
