<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once dirname(__FILE__) .'/../../../../config.php';
include_once dirname(__FILE__) .'/../../../../mqttchat.php';
$l_1=$language[20];
$l_2=$language[21];
$l_3=$language[22];
if(USE_FRIENDS==0){
$l_4=$language[23];
}else{
$l_4=$language[24];    
}
$l_5= $language[48];
$l_6= $language[49];
$l_7= $language[50];
$l_8= $language[65];
echo <<<EOD
<div id="mqttchat-contact-options-top">       
<ul class="mqttchat_window_options" contact-id="\${contact.id}">  
    {{if (contact.user.profile_link != "")}}<li class="mqttchat_cmd" action="link"><a href="\${contact.user.profile_link}">${l_8}</a></li>{{/if}}
    <li class="mqttchat_cmd" index=0><a href="#">{{if (contact.locked==0)}}${l_2}{{else}}${l_3}{{/if}}</a></li>
    <li class="mqttchat_cmd" index=1><a href="#">${l_4}</a></li>
    <li class="mqttchat_separator"></li>
    {{if (contact.micready==1)}}<li class="mqttchat_cmd" action="audio"><a href="#">${l_5}</a></li>{{/if}}
    {{if (contact.camready==1)}}<li class="mqttchat_cmd" action="video"><a href="#">${l_6}</a></li>{{/if}}
   
    {{each plugins}}
    {{if (\$value.config.USE_ICON==0)}} 
    {{if (\$index==1) }}
    <li class="mqttchat_separator"></li>
    {{/if}}
    <li class="mqttchat_cmd" pindex="\${\$index}"><a href="#">\${\$value.config.PLUGIN_LABEL}</a></li>
    {{/if}}
    {{/each}}    
    <li class="mqttchat_separator"></li>
    <li class="mqttchat_cmd" action="close"><a href="#">${l_7}</a></li>
    
</ul>  
</div>
EOD;
