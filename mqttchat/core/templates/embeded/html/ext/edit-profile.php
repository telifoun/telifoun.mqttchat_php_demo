<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once dirname(__FILE__) .'/../../../../../config.php';
include_once dirname(__FILE__) .'/../../../../../mqttchat.php';
$l_1=$language[37];
$l_2=$language[38];
$l_3=$language[39];
$l_4=$language[40];
$l_5=$language[41];
$l_6=$language[42];
$l_7=$language[43];
$l_8=$language[44];
echo <<<EOD
<div id="mqttchat-inner-content-div"> 
<form id="edit-profile-form">  
  
  <div class="mqttchat-user-avatar">
  <div id="mqttchat-avatar-preview" class="dropzone-previews dropzone"> 
   {{if (avatar_link!="")}}
    <img src="\${avatar_link}" />  
   {{/if}}
  </div> <!-- this is were the previews should be shown. -->
  <div class="mqttchat-tools">
         <div id="mqttchat-dropzone-upload" class="mqttchat-avatar-upload" title="${l_7}"></div>
         <div class="mqttchat-avatar-save" title="${l_8}"></div>
      </div>
  </div>

  <div class="mqttchat_chats_labels">${l_4}</div>  
  <div class="mqttchat-form-element"><select id="mqttchat-gender-select">
    <option value="0" {{if (gender==0)}} selected=true {{/if}}>${l_1}</option>
    <option value="1" {{if (gender==1)}} selected=true {{/if}}>${l_2}</option>
    <option value="2" {{if (gender==2)}} selected=true {{/if}}>${l_3}</option></select></div>

    
  <div class="mqttchat_chats_labels">${l_5}</div>  
  <div class="mqttchat-form-element"><input type="text" class="mqttchat-input" name="name" value="\${name}"  /></div>


  <div class="mqttchat_chats_labels">${l_6}</div>  
  <div class="mqttchat-form-element last"><input type="text" class="mqttchat-input" name="surname" value="\${surname}"/></div>

  
</form>
</div>

<script id="mqttchat-avatar-upload-tpl" type="text/x-jquery-tmpl"> 
   <div class="dz-preview dz-file-preview">
    <img data-dz-thumbnail />
    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
    <div class="dz-error-message mqttchat-edit-avatar-error"><span data-dz-errormessage></span></div>
   </div>
  </script>
  
EOD;
