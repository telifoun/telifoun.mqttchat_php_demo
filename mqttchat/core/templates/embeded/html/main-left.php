<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once dirname(__FILE__) .'/../../../../config.php';
include_once dirname(__FILE__) .'/../../../../mqttchat.php';
$search_users_li="";
$l_1=$language[27];
if(USE_FRIENDS==0){
 $search_users_li=' <li id="mqttchat_contacts_add" class="menu"></li>';
}
echo <<<EOD
<div id="mqttchat_select_menu">
    <ul id="mqttchat_select_menu_row">
    <li class="separator"></li>
    <li id="mqttchat_contacts_all" class="menu selected"></li>  
    <li id="mqttchat_contacts_online" class="menu"></li>      
    ${search_users_li}
    <li class="separator"></li>
    </ul>
</div>
  

<div id="mqttchat-left-content"></div> 
    
<div id="mqttchat_serachForm">
<input type="text" id="mqttchat_user_search_term" placeholder="${l_1}" autocomplete="off">
<button id="mqttchat-search-btn" class="search mqttchat-search-btn"></button>
</div>

EOD;

