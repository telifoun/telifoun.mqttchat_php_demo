<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once dirname(__FILE__) .'/../../../../config.php';
include_once dirname(__FILE__) .'/../../../../mqttchat.php';
$l_1=$language[45];
$l_2=$language[46];
echo <<<EOD
<div class="mqttchat-disconnect">
<div class="mqttchat-logo"></div>
<div class="mqttchat-disconnect-message">
    <span>${l_1}</span>
    <br><br><button id="mattchat-btn-on">${l_2}</button>       
</div>
</div>
EOD;
    