<?php

/* 
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * 
 * This is a paid script developed by Gaddour Mohamed. It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */
echo <<<EOD
  <div class="mqttchat-alert">
    <div class="mqttchat-logo"></div>  
    <h1>\${title}</h1>
    <div class="mqttchat-error-message">        
        {{html message}}
    </div> 
    <div class="mqttchat-error-actions">
    <a href="#" id="mqttchat-error-refrech" class="mqttchat-error-button">\${refech_label}</a>
    <a href="https://mqttchat.telifoun.com/index/error?domain=\${domain}" class="mqttchat-error-button" target="_blank">\${send_error_label}</a>
    </div>
</div>
EOD;


