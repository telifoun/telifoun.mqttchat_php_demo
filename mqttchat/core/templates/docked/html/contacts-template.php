<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once dirname(__FILE__) .'/../../../../config.php';
include_once dirname(__FILE__) .'/../../../../mqttchat.php';
$base_url=BASE_URL;
$l9=$language[9];
echo <<<EOD
 <li id="\${id}" {{if open==1}}class="opened"{{/if}} > 
   <div class="mqttchat-contact">

  <div class="mqttchat-contact-left">
    <div class="mqttchat-contact-user">
    <div class="mqttchat-contact-avatar">
    <img src="\${user.avatar_link}" class="mqttchat-avatar-img" alt="\${user.sstatus}"/>
    <span class="mqttchat-avatar-badge \${user.sstatus}"></span>
    </div>
    <div class="mqttchat-contact-infos">
       <div class="mqttchat_username">\${user.name} \${user.surname}</div>
       <div class="mqttchat_l_message">{{if typing==0}} {{if lastMessage != null}}  {{if (lastMessage.from==user.id && lastMessage.read==0)}} <b> {{/if}} {{html lastMessage.preview}} {{if (lastMessage.from==user.id && lastMessage.read==0)}} </b> {{/if}} {{/if}} {{else}}  <span class="mqttchat-contact-typing">${l9}</span> {{/if}}</div>
    </div> 
    </div>
    </div>   
    <div class="mqttchat-contact-right">
    <div class="mqttchat-contact-blocked {{if locked==1}}ok{{/if}}"></div> 
    <div class="mqttchat-contact-count">{{if count>0}} <span class="mqttchat-n-r-m-c"> {{if count>99}} 99+ {{else}} \${count} {{/if}}</span>{{/if}}</div>
    <div class="mqttchat-contact-options {{if running==1}}loiding{{/if}}"></div> 
    </div>
   </div>
 </li>
EOD;
