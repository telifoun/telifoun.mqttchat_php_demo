<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once dirname(__FILE__) .'/../../../../config.php';
include_once dirname(__FILE__) .'/../../../../mqttchat.php';
$userid= $_REQUEST["userid"];
$l_11=$language[11];
echo <<<EOD
{{each messages}}
<li id="\${\$value.id}" > 

<div class="mqttchat-message-sent-date {{if (\$value.send_date_label!="")}} ok {{/if}}">{{html \$value.send_date_label}}</div>

<div style="clear:both"></div>

<div  class="mqttchat-message-content 
     {{if (\$value.from==${userid})}}  mqttchat-message--sent 
     {{else}} mqttchat-message--recv 
     {{/if}}
     {{if (\$value.sent)}} ok {{/if}} 
     ">
{{if (\$value.type==0)}}
    <div  class="mqttchat-message__bubble  
          {{if (\$value.from==${userid})}}  mqttchat-message__bubble--sent {{if (\$index>0 && messages[\$index-1].from==${userid})}} sent {{/if}}
          {{else}}  mqttchat-message__bubble--rcvd  {{if (\$index>0 && messages[\$index-1].from!=${userid})}} rcvd {{/if}} {{/if}}
          {{if ((\$index == messages.length-1) || (messages[\$index+1].from!=\$value.from))}}  mqttchat-message__bubble--stop {{/if}}
          ">
   {{html \$value.text}} 
   </div>
{{else}}
   {{html \$value.text}}   
{{/if}}
   
</div>  
<div style="clear:both"></div>
<div class="mqttchat-message-info {{if (\$value.from==${userid}) && (\$value.read==1) && ((\$index == messages.length-1) || (messages[\$index+1].from==${userid} && (messages[\$index+1].read==0)))}} read {{/if}}">${l_11} {{html \$value.read_date}}</div>
</li>
{{/each}}
EOD;
          
