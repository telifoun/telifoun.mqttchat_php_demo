<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once dirname(__FILE__) .'/../../../../config.php';
echo <<<EOD
  <div class="mqttchat-contact-online">
    <div class="mqttchat-contact-online-avatar">
    <div class="mqttchat-contact-avatar">
    <img src="\${user.avatar_link}" class="mqttchat-avatar-img" alt="\${user.status}"/>
    <span class="mqttchat-avatar-badge \${user.sstatus}"></span>
    </div>
    </div>
    <div class="mqttchat-contact-online-infos">
    <div class="mqttchat_username">\${user.name} \${user.surname}</div>  
    </div>
</div>
EOD;

