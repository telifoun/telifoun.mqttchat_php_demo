<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */


include_once dirname(__FILE__) .'/../../../../config.php';
include_once dirname(__FILE__) .'/../../../../mqttchat.php';
$l_1=$language[32];
$l_2=$language[33];
$l_3=$language[3];
$l_4=$language[4];
$editProfileCMDHTML=(EDIT_PROFILE==1)?'<li class="mqttchat_cmd"><a href=""></a>'.$l_1.'</li>':'<li class="mqttchat_separator"></li>';
echo <<<EOD
<div id="mqttchat-connected-user">
  <div class="mqttchat-owner-avatar">
   <img src="\${avatar_link}" class="mqttchat-avatar-img" />
   <div class="mqttchat-avatar-badge \${ustatus}"></div>
  </div>
  <div class="mqttchat-owner-details">
     <div class="mqttchat-displayname">\${name} \${surname}</div>
     <div class="mqttchat_user_status">    
     <div class="mqttchat_userdisplaystatus">{{if (ustatus=='offline')}} Hors ligne {{else}} {{if (ustatus=='online')}}${l_3}{{else}}${l_4}{{/if}}{{/if}}</div>   
     <span class="mqttchat-status-arrow"></span>
     </div>
  </div>
</div> 

<script id="mqttchat-status-menu" type="text/x-jQuery-tmpl">
    <ul class="mqttchat_window_options">
     <li class="mqttchat_cmd"><a href=""><div class="mqttchat_status"><div class="mqttchat_status_option mqttchat_user_available"></div><div class="mqttchat_status_value">${l_3}</div></div></a></li>
     <li class="mqttchat_cmd"><a href=""><div class="mqttchat_status"><div class="mqttchat_status_option mqttchat_user_busy"></div><div class="mqttchat_status_value">${l_4}</div></div></a></li>
     <li class="mqttchat_separator"></li>
     ${editProfileCMDHTML}
     <li class="mqttchat_cmd"><a href=""></a>${l_2}</li>
    </ul>
</script>
EOD;
     