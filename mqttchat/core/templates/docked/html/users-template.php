<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once dirname(__FILE__) .'/../../../../config.php';
$base_url=BASE_URL;
echo <<<EOD
 <li id="\${id}"> 
   <div class="mqttchat-contact">
<div class="mqttchat-contact-left">
    <div class="mqttchat-contact-user">
    <div class="mqttchat-contact-avatar">
    <img src="\${avatar_link}" class="mqttchat-avatar-img" alt="\${sstatus}"/>
    <span class="mqttchat-avatar-badge \${sstatus}"></span>
    </div>
    <div class="mqttchat-user-infos">
       <div class="mqttchat_username">\${name} \${surname}</div>
       <div class="mqttchat-user-last-online">{{if sstatus!='online'}}\${t}{{/if}}</div>
    </div> 
    </div>
</div>
  </div>
 </li>
EOD;
