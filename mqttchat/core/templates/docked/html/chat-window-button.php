<?php

/* 
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * 
 * This is a paid script developed by Gaddour Mohamed. It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */
include_once dirname(__FILE__) .'/../../../../config.php';
?>
<div class="mqttchat-window-button-content">
    <div class="mqttchat_user_details"> 
        <div class="mqttchat_user_details_avatar"> 
            <img src="${contact.user.avatar_link}" class="mqttchat-avatar-img">  
            <span class="mqttchat-avatar-badge ${contact.user.sstatus}"></span>                 
        </div>                  
        <div class="mqttchat_user_details_details"> 
            <div class="mqttchat-content-top">  
                <div class="mqttchat_username">${contact.user.name} ${contact.user.surname}</div>                 
            </div>                          
        </div>             
    </div>  
    <div class="mqttchat_user_plugins"> 
        <div class="mqttchat-contact-count">{{if contact.count>0}} <span class="mqttchat-n-r-m-c"> {{if contact.count>99}} 99+ {{else}} ${contact.count} {{/if}}</span>{{/if}}</div>
        <div class="mqttchat_user_closewindow" id="mqttchat_closewindow" title="Close chat window">   
            <img src="<?=BASE_URL?>/images/remove.svg">  
        </div> 
    </div>
</div>
