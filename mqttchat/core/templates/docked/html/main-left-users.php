<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once dirname(__FILE__) .'/../../../../config.php';
include_once dirname(__FILE__) .'/../../../../mqttchat.php';
$l_1=$language[3];
$l_2=$language[29];
$l_3=$language[30];
$l_4=$language[31];
echo <<<EOD
<div class="mqttchat-section-users">
    <div class="mqttchat-users-data">        
    <div id="mqttchat-users-loiding" class="mqttchat-content-loading"></div>                
    <ul class="mqttchat_users"></ul>    
    </div>
    <div class="mqttchat-loadmore-users"></div>   



    <div class="mqttchat-users-filter-header">
            <div class="mqttchat-users-filter-header-title">${l_4}</div>
            <div class="mqttchat-users-filter-header-icon"></div>
    </div>
    <div class="mqttchat-users-filter">
        <div class="mqttchat-users-filter-content" id="mqttchat-users-filter-id">
            <div class="mqttchat-users-filter-form">
                  <div class="mqttchat_param_label">${l_1}
                  <div class="mqttchat_param_value">
                     <input type="checkbox" id="switcher4" class="mqttchat-filter-switch-check" value="online" checked="checked">
                  </div>
                  </div>
                  <div class="mqttchat_param_label">${l_2}
                  <div class="mqttchat_param_value">
                     <input type="checkbox" id="switcher5" class="mqttchat-filter-switch-check"  value="male" checked="checked">
                  </div>
                  </div>
                  <div class="mqttchat_param_label">${l_3}
                  <div class="mqttchat_param_value">
                     <input type="checkbox" id="switcher6" class="mqttchat-filter-switch-check" value="female" checked="checked">
                  </div>
                  </div>
            </div>   
           </div>       
        
    </div>
  </div>
EOD;
