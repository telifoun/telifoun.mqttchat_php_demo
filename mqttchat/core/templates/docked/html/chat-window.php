<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once dirname(__FILE__) .'/../../../../config.php';
include_once dirname(__FILE__) .'/../../../../mqttchat.php';
$l9=$language[9];
$maxlength=TEXTAREA_MAXLENGTH;
$l_4=$language[64];
$l_5=$language[66];
echo <<<EOD
<div id="mqttchat_window">  
        <div class="mqttchat_window_top"> 
        </div>       
        
        <div id="mqttchat_window_content_\${id}"  class="mqttchat_window_content">
            <div class="mqttchat-loadmore-messages"></div>
            <div class="mqttchat-section-messages">
                <div id="mqttchat-messages-loiding" class="mqttchat-content-loading"></div>  
                <ul class="mqttchat-messages"></ul>            
               <div class="mqttchat-message-typing">
               <span class="mqttchat-contact-typing">${l9}</span>
               </div>
            </div>
        </div>       
        
        
        <div class="mqttchat_window_footer"> 
          <div id="mqttchat_message_input">
           <textarea id="mqttchat_textarea" class="mqttchat_textarea" rows="1" maxlength="${maxlength}" placeholder="${l_4}"></textarea>
          </div>     

          <div id="mqttchat_plugins_menu">
             <ul>
             {{each plugins}}
               {{if (\$value.config.USE_ICON==1)}} 
               <li pindex="\${\$index}" title="\${\$value.config.PLUGIN_LABEL}"><div id="mqttchat_plugin_\${\$index}_icon" class="\${\$value.config.IDLE_ICON_CLASS}"></div></li>
               {{/if}}
             {{/each}}
               <li><div id="mqttchat_send_button" class="mqttchat-send-btn" title="${l_5}"></div></li>
             </ul>    
            </div> 
    
        </div>
        
        <div id="mqttchat_popup_window" class="mqttchat_popup_windows">
         <div class="mqttchat-content-loading"></div>    
         <div id="mqttchat_popup_content" ></div>   
        </div>
        
        <div id="mqttchat_plugin_window" class="mqttchat_plugin_windows">  
        <div class="mqttchat_plugin_window_title">
           <span class="mqttchat_plugin_window_name"></span>    
           <div class="mqttchat_plugin_window_closebox"></div>
        </div>
        <div class="mqttchat_plugin_window_content">
          <div class="mqttchat-content-loading"></div>   
          <div class="mqttchat_plugin_content"></div>   
        </div>            
        </div>         
</div>
               
<div class="mqttchat-window-button"></div>
EOD;
