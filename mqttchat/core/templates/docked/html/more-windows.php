<?php

/* 
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * 
 * This is a paid script developed by Gaddour Mohamed. It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */
?>
<div class="mqttchat-more-windows-list">    
    <ul class="mqttchat_window_options">
        {{each moreWindows }}
        <li class="mqttchat_cmd" id="${contact_index}" rel="${window_index}">
            <div class="mqttchat-contact-more"><div class="mqttchat-contact-more-name">${name}{{if count>0}} ({{if count>99}}99+{{else}}${count}{{/if}}){{/if}}</div><div class="mqttchat-remove-more"></div></div></li>
        {{/each}}
    </ul>
</div>
