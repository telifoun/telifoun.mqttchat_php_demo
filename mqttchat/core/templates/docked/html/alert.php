<?php

/* 
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * 
 * This is a paid script developed by Gaddour Mohamed. It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */
include_once dirname(__FILE__) .'/../../../../config.php';
include_once dirname(__FILE__) .'/../../../../mqttchat.php';
$l_1=$language[67];

$template= <<<EOD
<div class="mqttchat-docker">

<div class="mqttchat-alert-button" style="width:\${button_w}px;">
    <div class="mqttchat-global-label">${l_1}</div><div class="mqttchat-global-notification"><div class="mqttchat-icon-warning"></div></div>
</div>
 
<div class="mqttchat-alert-popup">
  <div class="mqttchat-alert-popup-header">
        <div class="mqttchat-alert-popup-header-left">
         <div class="mqttchat-logo"></div> 
        </div>
        <div class="mqttchat-alert-popup-header-right">
         <button class="mqttchat-header-btn" id="mqttchat-alert-close">
                 <svg fill="none" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg" class="mqttchat-svg"><path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"></path></svg>
               </button>       
        </div>
</div>
  <div class="mqttchat-alert">    
    <h1>\${title}</h1>
    <div class="mqttchat-error-message">        
        {{html message}}
    </div> 
    <div class="mqttchat-error-actions">
    <div  class="mqttchat-error-button"><a href="#" id="mqttchat-error-refrech">\${refech_label}</a></div>
    <div  class="mqttchat-error-button"><a href="https://mqttchat.telifoun.com/index/error?domain=\${domain}" target="_blank">\${send_error_label}</a></div>
    </div>
</div>
</div>
</div>
EOD;


header('Content-type: application/json; charset=utf-8');
if(isset($_GET["callback"])){
echo $_GET["callback"] . "(". json_encode($template) .")";
}else{
echo json_encode($template) ;   
}
exit;


