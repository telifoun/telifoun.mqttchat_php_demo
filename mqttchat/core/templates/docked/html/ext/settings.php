<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once dirname(__FILE__) .'/../../../../../config.php';
include_once dirname(__FILE__) .'/../../../../../mqttchat.php';
$l_1=strtoupper($language[2]);
$l_2=$language[3];
$l_3=$language[4];
$l_4=strtoupper($language[1]);
$l_5=$language[17];
$l_6=$language[18];
$l_7=$language[19];
echo <<<EOD
    
    <div id="mqttchat-inner-content-div"> 
        
    <div class="mqttchat_chats_labels">${l_1}</div>
    
    <div class="mqttchat_optionstatus">
            <div class="mqttchat_optionstatus2 mqttchat_user_available"></div>
            <div style="margin-top:3px;">${l_2}</div>
            <label class="mqttchat_radio">
            <input id="mqttchat_statusavailable_radio" type="radio" name="mqttchat_statusoptions" value="online" {{if (user_status=='online')}}checked{{/if}} >
            <span class="mqttchat_radio_outer"><span class="mqttchat_radio_inner"></span></span>
            </label>
    </div>
    <div class="mqttchat_optionstatus">
            <div class="mqttchat_optionstatus2 mqttchat_user_busy"></div>
            <div style="margin-top:3px;">${l_3}</div>
            <label class="mqttchat_radio">
            <input id="mqttchat_statusavailable_radio" type="radio" name="mqttchat_statusoptions" value="busy" {{if (user_status=='busy')}}checked{{/if}}>
            <span class="mqttchat_radio_outer"><span class="mqttchat_radio_inner"></span></span>
            </label>
    </div>  
    
   
    <div class="mqttchat_chats_labels">${l_4}</div>
     
     <div class="mqttchat_lightdisplay">
         <div class="mqttchat_param_label">${l_5}
         <div class="mqttchat_param_value">
             <input type="checkbox" id="switcher1" class="mqttchat-settings-switch-check" value="sound_notif" checked="checked">
         </div>
         </div>
     </div>   
         
      <div class="mqttchat_lightdisplay">
         <div class="mqttchat_param_label">${l_6}
         <div class="mqttchat_param_value">
             <input type="checkbox" id="switcher2" class="mqttchat-settings-switch-check" value="ac_ok" checked="checked">
         </div>
         </div>
     </div>    
         
         
      <div class="mqttchat_lightdisplay">
         <div class="mqttchat_param_label">${l_7}
         <div class="mqttchat_param_value">
             <input type="checkbox" id="switcher2" class="mqttchat-settings-switch-check" value="vc_ok" checked="checked">
         </div>
         </div>
     </div>    
     
     
</div>
EOD;
            