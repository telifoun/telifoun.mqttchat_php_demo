<?php

/* 
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * 
 * This is a paid script developed by Gaddour Mohamed. It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once dirname(__FILE__) .'/../../../../config.php';
include_once dirname(__FILE__) .'/../../../../mqttchat.php';
$base_url=BASE_URL;
$l_1= $language[48];
$l_2= $language[49];
$l_3= $language[50];
echo <<<EOD
 <div class="mqttchat_window_top_content">
             <div class="mqttchat_user_details">
                <div class="mqttchat_user_details_avatar">
                    <img src="\${contact.user.avatar_link}" class="mqttchat-avatar-img">
                    <span class="mqttchat-avatar-badge \${contact.user.sstatus}"></span>
                </div>
                <div class="mqttchat_user_details_details">                    
                        <div class="mqttchat-content-top">
                        <div class="mqttchat_username">\${contact.user.name} \${contact.user.surname}</div>
                        <div class="mqttchat_top_contact_options"></div>
                        </div>     
                        <div class="mqttchat_username_infos"><span class="mqttchat-top-contact-last-online"></span></div>
                   
                </div>
            </div>               
            
            <div class="mqttchat_user_plugins">
                {{if ((contact.user.sstatus!='offline') || (contact.user.sstatus=='background')) }}
                {{if (contact.micready==1)}}<div class="mqttchat_user_plugin" id="mqttchat_audiocall_icon" title="${l_1}"></div>{{/if}}
                {{if (contact.camready==1)}}<div class="mqttchat_user_plugin" id="mqttchat_videocall_icon" title="${l_2}"></div>{{/if}}
                {{/if}}
                <div class="mqttchat_user_closewindow" id="mqttchat_closewindow" title="${l_3}">
                <img src="${base_url}/images/remove.svg"/>
                </div>                
            </div>  
         </div>
EOD;
