<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * 
 * This is a paid script developed by Gaddour Mohamed. It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */
include_once dirname(__FILE__) .'/../../../../config.php';
include_once dirname(__FILE__) .'/../../../../mqttchat.php';
?>

<div class="mqttchat-docked">

<div class="mqttchat-users-button" style="width:${button_w}px;">
    <div class="mqttchat-global-label"><?=$language[67]?> <span id="mqttchat-global-label-status"></span></div><div class="mqttchat-global-notification"><span class="mqttchat-n-r-m-c"></span></div>
</div>
 
<div id="mqttchat-contacts-popup" class="mqttchat-users-popup">

    <div class="mqttchat-users-popup-header">
        <div class="mqttchat-users-popup-header-left"></div>
        <div class="mqttchat-users-popup-header-right">
            
               <button class="mqttchat-header-btn" id="mqttchat-main-settings">
                 <img src="<?=BASE_URL?>/images/settings.svg">
               </button>
            
               <button class="mqttchat-header-btn" id="mqttchat-main-close">
                 <svg fill="none" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg" class="mqttchat-svg"><path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"></path></svg>
               </button>
       
        </div>
    </div>
    <div class="mqttchat-users-popup-content">         
    </div>


    
<div id="mqttchat_ext_panel" class="mqttchat_ext_windows"> 
        <div id="mqttchat_windows_titlebar">  
            <div id="mqttchat-windows-title"><span>test-title</span></div>  
            <div id="mqttchat-windows-close"><img src="<?=BASE_URL?>/images/removewhite.svg"/></div>  
       </div>
       <div class="mqttchat_ext_windows_content">
       <div class="mqttchat-content-loading"></div>  
       <div id="mqttchat_ext_content"></div>
       </div>
</div>
    
</div>
<div id="mqttchat-more-windows-button" class="mqttchat-more-button">
        
</div>
<div id="mqttchat-visio-element" style="display:none;"></div>    
</div>