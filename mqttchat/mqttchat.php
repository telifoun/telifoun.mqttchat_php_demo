<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

if(session_id() == '') {
  session_start();
} 

$cookiePrefix = ''; 
define ("AVATAR_MEN_URL",BASE_URL."/images/avatar-men.png");
define ("AVATAR_WOMEN_URL",BASE_URL."/images/avatar-women.png");

include_once(dirname(__FILE__) .DIRECTORY_SEPARATOR.'mqttchat_hooks.php');

date_default_timezone_set('UTC');
if(LANG !="auto"){
 $lang=LANG;   
}else{
  if (!empty($_COOKIE[$cookiePrefix."lang"])) {
   $lang = preg_replace("/[^A-Za-z0-9\-]/", '', $_COOKIE[$cookiePrefix . "lang"]);
  }else if (!empty($_SESSION["lang"])){
   $lang = $_SESSION["lang"];
  }else{
   $lang = "en";
  }
}

include_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'languages'.DIRECTORY_SEPARATOR.'en.php');

if (file_exists(dirname(__FILE__).DIRECTORY_SEPARATOR.'languages'.DIRECTORY_SEPARATOR.$lang.'.php')) {
  include_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'languages'.DIRECTORY_SEPARATOR.$lang.'.php');
}


if($rtl=='1'){
$left = 'right';
$right = 'left';   
$font_size='1em';
$dir='rtl';
}else{
$left = 'left';
$right = 'right';
$font_size='0.99em';
$dir='ltr';
}


if(get_magic_quotes_runtime()) {
 set_magic_quotes_runtime(false);
}

ini_set('log_errors', 'Off');
ini_set('display_errors','Off');


if (defined('ERROR_LOGGING') && ERROR_LOGGING == '1') {
    error_reporting(E_ALL);
    ini_set('error_log', 'log'.DIRECTORY_SEPARATOR.'error.log');
    ini_set('log_errors','On');
}

if (defined('DEV_MODE') && DEV_MODE == '1') {
    error_reporting(E_ALL);
    ini_set('display_errors','On');
}


foreach($_REQUEST as $key => $val){
    if ($key != 'message' && $key != 'statusmessage') {
        $_REQUEST[$key] = str_replace('<','',str_replace('"','',str_replace("'",'',str_replace('>','',$_REQUEST[$key]))));
        if(!empty($_POST[$key])){
                $_POST[$key] = str_replace('<','',str_replace('"','',str_replace("'",'',str_replace('>','',$_POST[$key]))));
        }
        if(!empty($_GET[$key])){
                $_GET[$key] = str_replace('<','',str_replace('"','',str_replace("'",'',str_replace('>','',$_GET[$key]))));
        }
        if(!empty($_COOKIE[$key])){
                $_COOKIE[$key] = str_replace('<','',str_replace("'",'',str_replace('>','',$_COOKIE[$key])));
        }
    }
}

if (get_magic_quotes_gpc() || (defined('FORCE_MAGIC_QUOTES') && FORCE_MAGIC_QUOTES == 1)) {
   $_GET = stripSlashesDeep($_GET);
   $_POST = stripSlashesDeep($_POST);
   $_REQUEST = stripSlashesDeep($_REQUEST);
   $_COOKIE = stripSlashesDeep($_COOKIE);
}


/** get modules **/
$modules_js = "this.modules=[";
if(isset($modules) && sizeof($modules)>0){    
    foreach($modules as $key => $module){  
     $modules_js.="$.".$module;
     if($key<sizeof($modules)-1){
       $modules_js.=",";  
     }
    }
}
$modules_js.="];";

/** get plugins **/
$plugins_js = "this.plugins={";
if(isset($plugins) && sizeof($plugins)>0){  
    $i=0;
    foreach($plugins as $key => $plugin){  
     $plugins_js.=$key.":$.".$plugin;
     if($i<sizeof($plugins)-1){
       $plugins_js.=",";  
     }
     $i++;
    }
}
$plugins_js.="};";

/** init mqttchat userid **/
$MQTTCHAT_USERID_INIT=0; 
if(isset($_REQUEST["userid"]) && $_REQUEST["userid"]>0){
 $MQTTCHAT_USERID_INIT=$_REQUEST["userid"];
}

/** init hooks **/
mqttchat_init();



    