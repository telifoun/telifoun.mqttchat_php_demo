<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */


define('DB_SERVER','');
define('DB_PORT','3306');
define('DB_USERNAME','');
define('DB_PASSWORD','');
define('DB_NAME','');

if(!function_exists("mysqli_connect")){

	function mysqli_connect($db_server,$db_username,$db_password,$db_name,$port){
		return mysql_connect($db_server.':'.$port,$db_username,$db_password);
	}

	function mysqli_real_escape_string($dbh,$userid){
		return mysql_real_escape_string($userid);
	}

	function mysqli_select_db($dbh,$db_name){
		return mysql_select_db($db_name,$dbh);
	}

	function mysqli_connect_errno($dbh){
		return !$dbh;
	}

	function mysqli_query($dbh,$sql){
		return mysql_query($sql);
	}

	function mysqli_multi_query($dbh,$sql){
		$sqlarr = explode(';', $sql);
		foreach ($sqlarr as $sql){
			mysql_query($sql);
		}
		return true;
	}

	function mysqli_error($dbh){
		return mysql_error();
	}

	function mysqli_fetch_assoc($query){
		return mysql_fetch_assoc($query);
	}

	function mysqli_insert_id($dbh){
		return mysql_insert_id();
	}

	function mysqli_num_rows($query){
		return mysql_num_rows($query);
	}

	function mysqli_affected_rows($query){
		return mysql_affected_rows($query);
	}
}

/**
 * 
 * @global type $dbh
 */
function MysqlDBConnect()
{
	global $dbh;
	$port = DB_PORT;
	if(empty($port)){
		$port = '3306';
	}
	$dbserver = explode(':',DB_SERVER);
	if(!empty($dbserver[1])){
	    $port = $dbserver[1];
	}
	$db_server = $dbserver[0];
	$dbh = mysqli_connect($db_server,DB_USERNAME,DB_PASSWORD,DB_NAME,$port);
	if (mysqli_connect_errno($dbh)) {
		$dbh = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_NAME,$port,'/tmp/mysql5.sock');
	}

	if (mysqli_connect_errno($dbh)) {
		echo "<h3>Unable to connect to database due to following error(s). Please check details in configuration file.</h3>";
		if (!defined('DEV_MODE') || (defined('DEV_MODE') && DEV_MODE != '1')){
			ini_set('display_errors','On');
			echo mysqli_connect_error($dbh);
			ini_set('display_errors','Off');
		}
		exit();
	}

	mysqli_select_db($dbh,DB_NAME);
	mysqli_query($dbh,"SET NAMES utf8");
	mysqli_query($dbh,"SET CHARACTER SET utf8");
	mysqli_query($dbh,"SET COLLATION_CONNECTION = 'utf8_general_ci'");
}

/**
 * callback ok 
 * @return type
 */
function cb_ok(){
  return array("ok"=>true);  
}
/**
 * callback ko 
 * @param type $error
 * @return type
 */
function cb_ko($error){
  return array("ok"=>false,"error"=>$error); 
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

 /* HOOKS */

function mqttchat_init(){
 //MysqlDBConnect();   
}

/**
 * 
 * @param type $userid
 * @param type $name
 * @param type $ext
 * @param type $size
 * @param type $type [disk,cam]
 * @param type $pathMin
 * @param type $pathMax
 * @param type $urlMin
 * @param type $urlMax
 * @return type cb_ok()|cb_ko($error)
 */
function  mqttchat_photo_upload($userid,$name,$ext,$size,$type,$pathMin,$pathMax,$urlMin,$urlMax){
  // your custom code here      
  return cb_ok();
}


/**
 * 
 * @param type $userid
 * @param type $name
 * @param type $size
 * @param type $path
 * @param type $url
 * @return type cb_ok()|cb_ko($error)
 */
function  mqttchat_record_upload($userid,$name,$size,$path,$url){
  // your custom code here  
  return cb_ok();
}



/**
 * 
 * @param type $userid
 * @param type $old_avatar_min_path
 * @param type $old_avatar_max_path
 * @param type $new_avatar_url
 */
function mqttchat_avatar_update($userid,$old_avatar_min_path,$old_avatar_max_path,$new_avatar_url){  
       unlink($old_avatar_min_path);   
       unlink($old_avatar_max_path);
}


/**
 * 
 * @param type $userid
 * @param type $name
 * @param type $surname
 * @param type $gender
 */
function mqttchat_profile_update($userid,$name,$surname,$gender){
  // your custom code here    
}

/**
 * You must first activate mqttchat_status_callback in Admin Panel 
 * before you can use mqttchat_user_status_update hook.
 */
/**
 * 
 * @param type $userid
 * @param type $status
 * @param type $unix_timestamp
 * 
 */
function mqttchat_user_status_update($userid,$status,$unix_timestamp){ 
  // your custom code here   
}
