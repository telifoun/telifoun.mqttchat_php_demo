<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once(dirname(__FILE__) .DIRECTORY_SEPARATOR.'config.php');
include_once(dirname(__FILE__) .DIRECTORY_SEPARATOR.'shared_f.php');
include_once(dirname(__FILE__) .DIRECTORY_SEPARATOR.'mqttchat.php');
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR. '/sdk/vendor/autoload.php');


$return=ko(0,$language[5]);

$user =new telifoun\mqttchat\user();
$userid=$_REQUEST['userid'];

if(isset($userid)){  
    

    $user->_setUserid($userid);    
    $return =null;
   
    if(isset($_REQUEST["sound_notif"]) && $_REQUEST["sound_notif"]>=0){
    $return=$user->updateSettings(array("sound_notif"=>$_REQUEST["sound_notif"]));  
    }
    
    if(isset($_REQUEST["ac_ok"]) && $_REQUEST["ac_ok"]>=0){
    $return=$user->updateSettings(array("ac_ok"=>($_REQUEST["ac_ok"])?1:0));  
    }
    
    if(isset($_REQUEST["vc_ok"]) && $_REQUEST["vc_ok"]>=0){
    $return=$user->updateSettings(array("vc_ok"=>($_REQUEST["vc_ok"])?1:0));  
    } 
    
    if(is_null($return)){
    $return=$user->getSettings();
    }
    
}
 
header('Content-type: application/json; charset=utf-8');
echo json_encode($return);
exit;