<?php


/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once dirname(__FILE__) .DIRECTORY_SEPARATOR.'/../../config.php';
include_once dirname(__FILE__) .DIRECTORY_SEPARATOR.'/../../mqttchat.php';

ob_start();
include_once dirname(__FILE__) .DIRECTORY_SEPARATOR.'/themes/main.css';
$css = ob_get_clean();


header('Content-type: text/css;charset=utf-8');
echo $css;