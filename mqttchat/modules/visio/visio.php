<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once dirname(__FILE__) .DIRECTORY_SEPARATOR.'config.php';

include_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'languages'.DIRECTORY_SEPARATOR.'en.php');
if (file_exists(dirname(__FILE__).DIRECTORY_SEPARATOR.'languages'.DIRECTORY_SEPARATOR.$lang.'.php')) {
include_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'languages'.DIRECTORY_SEPARATOR.$lang.'.php');
}

ob_start();

if(USE_TURN==1){
$visio_config="var visioConfig={servers:[{urls: 'turn:".TURN_SERVER."',
    username: '".TURN_USER."',
    credential: '".TURN_PASSWORD."'}]};";
}else{
$visio_config="var visioConfig={};";    
}


include_once dirname(__FILE__). '/visio-js.min.js';


$js = ob_get_clean();
header('Content-type: text/javascript;charset=utf-8');
echo $js;