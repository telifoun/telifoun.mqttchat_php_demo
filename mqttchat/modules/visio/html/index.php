<?php
include_once dirname(__FILE__) ."/../../../config.php";
include_once dirname(__FILE__) ."/../../../mqttchat.php";
include_once dirname(__FILE__) ."/../config.php";
require_once (dirname(__FILE__). '/../../../sdk/vendor/autoload.php');
?>

<html>
    <head>
        <title>MQTTCHAT VISIO</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="<?=BASE_URL?>/modules/visio/css.php" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<?=BASE_URL?>/js/adapter.js"></script>   
        <meta name="viewport" content="width=device-width, initial-scale=1">   
    </head>
    <body style="margin:0;padding:0;">               
    
         <?php
         $userid= isset($_REQUEST["userid"])?$_REQUEST["userid"]:0;
         if($userid==0){
           echo "Missing userid !!!";  
           return;
         }
         $action = isset($_REQUEST["action"])?$_REQUEST["action"]:"audio"; 
         ?>              
            <div class="mqttchat-visio-container  <?=$action?>">                
                
                <div class="mqttchat-visio-header"></div> 
                
                
                <div class="mqttchat-visio-middle">                    
                   
                       
                       <?php
                       $user =new telifoun\mqttchat\user();    
                       $user->_setUserId($userid);
                       $result=$user->Get();
                       if($result["ok"]){
                       if(isset($result["response"]["avatar_link"]) && !empty($result["response"]["avatar_link"])){
                           $avatar_link=$result["response"]["avatar_link"];
                       }else{
                          if($result["response"]["gender"]>0){
                            $avatar_link=AVATAR_WOMEN_URL;  
                          }else{
                            $avatar_link=AVATAR_MEN_URL;   
                          }
                       }
                       ?>
                       <div class="mqttchat-visio-middle-user">
                           <div class="avatar"><img src="<?=$avatar_link?>"></div>
                           <div class="name"> <?=$result["response"]["name"]?>  <?=$result["response"]["surname"]?></div>
                       </div>
                        <?php
                       } else{
                       echo $result["response"]["error"];    
                       }        
                       ?>                       
                       <div id="mqttchat-visio-message" class="mqttchat-visio-middle-message"> </div>
                       
                   
                </div>    
                <div class="mqttchat-visio-footer">                    
                        <div id ="mqttchat-visio-call-btn" class="btn call"></div>
                        <div id ="mqttchat-visio-mic-btn" class="btn mic"></div>  
                        <div id ="mqttchat-visio-hangup-btn" class="btn hangup"></div>  
                        <div id ="mqttchat-visio-close-btn" class="btn close"></div> 
                </div>                    
                    
             </div>
                
                <?php
                if ($action=="audio"){
                ?>
                <div class="mqttchat-audio-tag"><audio id="mqttchat-audio-player"></audio></div>
                <?php
                } else {
                ?>
                <div class="mqttchat-video-tag">
                    <video id="mqttchat-video-player-remote"></video>
                    <video id="mqttchat-video-player-local"></video>
                </div>
                <?php
                }
                ?>
                
            </div>   
           
    </body> 
</html>
