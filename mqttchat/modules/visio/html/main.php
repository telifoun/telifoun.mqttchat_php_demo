<?php

/* 
 * Copyright (C) 2018 Mohamed
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
//"http://10.0.0.100/MQTTchat/photos/138/MIN/138_1539961694478.jpg"
?>

<div class="mqttchat-visio-dialog">
    
    <div class="mqttchat-visio-content">
        
    <div class="mqttchat-visio-dialog-title">
        <span class="mqttchat-visio-dialog-title-label">{{if action=="audio"}} ${l_1} {{else}} ${l_2} {{/if}} </span><span id="mqttchat-visio-close-btn" class="mqttchat-close-btn"></span>      
    </div>
    
    <div class="mqttchat-visio-dialog-content">
        <div class="mqttchat-visio-middle-user">            
           <div class="avatar"><img src="${user.avatar_link}"><span class="mqttchat-visio-badge ${action}"></span></div>
           <div class="name">${user.name} ${user.surname}</div>
        </div>
        
    </div>    
    
    <div class="mqttchat-visio-dialog-footer">
        <div id="mqttchat-visio-button-accept" class="mqttchat-visio-button accept">${l_3} </div>  
        <div id="mqttchat-visio-button-reject" class="mqttchat-visio-button reject">${l_4} </div>         
    </div>
        
    </div>
    
    
</div>

