<?php

/* 
 * Copyright (C) 2018 mg73463
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
$rtl = '0';
$plugin_language[0]="Mettre fin à la communication actuelle?";
$visio_language[1]="Appel en cours ...";
$visio_language[2]="Appel manqué";
$visio_language[3]="Utilisateur occupé";
$visio_language[4]="En attente de flux à distance ...";
$visio_language[5]="Appel établi";
$visio_language[6]="Appel terminé";
$visio_language[7]="Oops une erreur est survenue :";
$visio_language[8]="Utilisateur injoignable";
$visio_language[9]="Utilisateur n'a pas répondu à temp";
$visio_language[10]="Appel audio reçu";
$visio_language[11]="Appel video reçu";
$visio_language[12]="Accepter";
$visio_language[13]="Refuser";

