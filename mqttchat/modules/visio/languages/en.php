<?php

/* 
 * Copyright (C) 2018 mg73463
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

$rtl = '0';
$visio_language[0]="End current communication ?";
$visio_language[1]="Calling ...";
$visio_language[2]="Dismissed Call";
$visio_language[3]="Busy User";
$visio_language[4]="Waiting remote stream ...";
$visio_language[5]="Established Call";
$visio_language[6]="Ended Call";
$visio_language[7]="Oops an error has occurred :";
$visio_language[8]="Unreachable User";
$visio_language[9]="User did not respond in time";
$visio_language[10]="Incoming Audio Call";
$visio_language[11]="Incoming Video Call";
$visio_language[12]="Accept";
$visio_language[13]="Reject";

