<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once(dirname(__FILE__) .DIRECTORY_SEPARATOR.'config.php');
include_once(dirname(__FILE__) .DIRECTORY_SEPARATOR.'shared_f.php');
include_once(dirname(__FILE__) .DIRECTORY_SEPARATOR.'mqttchat.php');
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR. '/sdk/vendor/autoload.php');

$return=ko(0,$language[5]);

$user =new telifoun\mqttchat\user();
$userid=$_REQUEST['userid'];

if(isset($userid)){ 
     
    $startIndex=0;$offset=AJAX_REQUEST_OFFSET;$term="";$online="1";$male="1";$female="1";
    
    if(isset($_REQUEST["startIndex"]) && $_REQUEST["startIndex"]>0){ 
      $startIndex=  $_REQUEST["startIndex"];
    }    
    if(isset($_REQUEST["offset"]) && $_REQUEST["offset"]>0){ 
      $offset=  $_REQUEST["offset"];
    } 
    if(isset($_REQUEST["term"]) && !empty($_REQUEST["term"])){ 
      $term=  $_REQUEST["term"];
    } 
    if(isset($_REQUEST["online"])){ 
      $online=  $_REQUEST["online"];
    }    
    if(isset($_REQUEST["male"])){ 
      $male=  $_REQUEST["male"];
    } 
    if(isset($_REQUEST["female"])){ 
      $female=  $_REQUEST["female"];
    }  
    
    $user->_setUserid($userid);
    $return =$user->getAllUsers($term, $online, $male, $female, $startIndex , $offset);
}
 
header('Content-type: application/json; charset=utf-8');
echo json_encode($return);
exit;