<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once(dirname(__FILE__) .DIRECTORY_SEPARATOR.'config.php');
include_once(dirname(__FILE__) .DIRECTORY_SEPARATOR.'shared_f.php');
include_once(dirname(__FILE__) .DIRECTORY_SEPARATOR.'mqttchat.php');
require_once (dirname(__FILE__).DIRECTORY_SEPARATOR. '/sdk/vendor/autoload.php');

$return =  ko(0,$language[5]) ;
$user =new telifoun\mqttchat\user();
$userid=$_REQUEST['l_userid'];

if(isset($userid)){

    $user->_setUserid($userid);
    $result=$user->Connect();   
    if($result["ok"]){
    $data=array("userid"=>$result["response"]['userid'], 
                "connection_id"=>$result["response"]['connection_id'],
                "device_id"=>$result["response"]['device_id'],            
                "hash"=>$result["response"]['mqtt_token'],
                "user_status"=>$result["response"]["user_status"]
                );
    $return = ok($data); 
    }else{
    $return =  ko($result["response"]["code"],$result["response"]["error"]) ;    
    }
}


header('Content-type: application/json; charset=utf-8');
echo json_encode($return);
exit;