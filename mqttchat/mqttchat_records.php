<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once (dirname(__FILE__) .DIRECTORY_SEPARATOR.'config.php'); 
include_once (dirname(__FILE__) .DIRECTORY_SEPARATOR.'shared_f.php');
include_once (dirname(__FILE__) .DIRECTORY_SEPARATOR.'mqttchat.php');

$return=ko(-1,$language[34]); 

$userid=$_REQUEST['userid'];

if ((!empty($_FILES)) && (isset($userid))) {
    
 try{
       
 /** init target path **/
 $targetPath = dirname( __FILE__ ) . DIRECTORY_SEPARATOR.  'records' . DIRECTORY_SEPARATOR;  
 
  /** create target path **/
 if (!file_exists($targetPath.$userid. DIRECTORY_SEPARATOR)) {
   mkdir($targetPath.$userid. DIRECTORY_SEPARATOR, 0777, true);   
 }
 
 $audio_result=array();
 
 $tempFile = $_FILES['audio']['tmp_name'] ;
 
 $filename=$targetPath.$userid.DIRECTORY_SEPARATOR.$userid.$_FILES['audio']['name'];
 
 $move_result=move_uploaded_file($tempFile,$filename);
 if(!$move_result){
  $return=ko(0,"Not uploaded because of error #".$_FILES["audio"]["error"]);    
                      
 }else{
 
     if(file_exists($filename)){

       $url=BASE_URL. "/records/".$userid."/".$userid.$_FILES['audio']['name'];
       $r=mqttchat_record_upload($userid,
                               $userid.$_FILES['audio']['name'],
                               $_FILES['audio']['size'],
                               $filename,
                               $url);
        if($r["ok"]){     
         $return=ok(array("url"=>$url));   
        }else{
         $return=ko(0,$r["error"]);      
        }        

     }
 }
 
 
}catch(\Exception $ex){    
 $return=ko($ex->getCode(),$ex->getMessage());     
}

}

header('Content-type: application/json; charset=utf-8');
echo json_encode($return);
exit;


   