<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace telifoun\mqttchat;
 
 class config{
     
    /** telifoun mqttchat app_id **/
    const APP_ID="";
    /** telifoun mqttchat app_secret **/
    const APP_SECRET="";  
    
    /** telifoun mqttchat rest server **/
    const MQTTCHAT_REST_SERVER="http://cluster1.telifoun.com/rest" ;
     
    /** telifoun mqttchat oauth2 server url **/
    const OAUTH2_TOKEN_URL="/oauth2/token";
    
    const REST_USERS_URL ="/v1.0/users";

    const REST_LOGIN_URL="/v1.0/login";    
    
    const REST_LOGOUT_URL="/v1.0/logout";
    
    const REST_FRIENDS_URL="/v1.0/friends";
    
    const REST_MESSAGES_URL="/v1.0/messages";
    
    const REST_CONTACTS_URL="/v1.0/contacts";
    
    const REST_SETTINGS_URL="/v1.0/settings";
    
    const REST_ACTIONS_URL="/v1.0/actions";
    
    const REST_CHANNELS_URL="/v1.0/channels";
    
    const REST_DEVICES_URL="/v1.0/devices";
     
 }