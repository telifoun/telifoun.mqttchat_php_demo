<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once(dirname(__FILE__) .DIRECTORY_SEPARATOR.'config.php');
include_once(dirname(__FILE__) .DIRECTORY_SEPARATOR.'shared_f.php');
include_once(dirname(__FILE__) .DIRECTORY_SEPARATOR.'mqttchat.php');
require_once (dirname(__FILE__).DIRECTORY_SEPARATOR. '/sdk/vendor/autoload.php');

$return=ko(0,$language[5]);

$user =new telifoun\mqttchat\user();
$userid=$_REQUEST['userid'];

if(isset($userid)){ 
   
 if(isset($_REQUEST["old_avatar_link"]) && 
                isset($_REQUEST["new_avatar_link"])){
     
   $user->_setUserid($userid);
   
   $new_avatar_link= $_REQUEST["new_avatar_link"];   
   $user->_setAvatarLink($new_avatar_link);
   $return=$user->Update();  
   
   if($return["ok"]){
    $old_avatar_link= $_REQUEST["old_avatar_link"];   
    $base_avatar_url= dirname(__FILE__) .DIRECTORY_SEPARATOR."photos".DIRECTORY_SEPARATOR.$userid;
    $image_name=parse_photo_in_url($old_avatar_link);
    if($image_name!=null){
    $min_image_url=$base_avatar_url.DIRECTORY_SEPARATOR."MIN".DIRECTORY_SEPARATOR.$image_name;
    $max_image_url=$base_avatar_url.DIRECTORY_SEPARATOR."MAX".DIRECTORY_SEPARATOR.$image_name;
    if(file_exists($min_image_url) && file_exists($max_image_url)){
    mqttchat_avatar_update($userid,$min_image_url,$max_image_url,$new_avatar_link);
    }
    }
  }
  
 }else{
  $return =  ko(-1,$language[34]) ; 
 }   
}  


header('Content-type: application/json; charset=utf-8');
echo json_encode($return);
exit;