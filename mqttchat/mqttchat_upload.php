<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once (dirname(__FILE__) .DIRECTORY_SEPARATOR.'config.php'); 
include_once (dirname(__FILE__) .DIRECTORY_SEPARATOR.'shared_f.php');
include_once (dirname(__FILE__) .DIRECTORY_SEPARATOR.'mqttchat.php');
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . '/sdk/vendor/autoload.php');

$return=ko(-1,$language[34]);

$userid=$_REQUEST['userid'];


if (!empty($_FILES) && (isset($userid))) {
    
 try{
      
    /** get photo type from POST **/
    $type=(!empty($_POST) && isset($_POST['type']))?$_POST['type']:'disk';   
    
    /** init local target path **/
    $targetPath = dirname( __FILE__ ) . DIRECTORY_SEPARATOR. 'photos' ;  
    
    /** create target path **/
    if (!file_exists($targetPath. DIRECTORY_SEPARATOR."$userid")) {
      mkdir($targetPath. DIRECTORY_SEPARATOR."$userid", 0777, true);   
    }
    
    if (!file_exists($targetPath.DIRECTORY_SEPARATOR."$userid".DIRECTORY_SEPARATOR."MIN")) {
      mkdir($targetPath.DIRECTORY_SEPARATOR."$userid". DIRECTORY_SEPARATOR."MIN", 0777, true);   
    }
    
    if (!file_exists($targetPath.DIRECTORY_SEPARATOR."$userid". DIRECTORY_SEPARATOR."MAX")) {
      mkdir($targetPath.DIRECTORY_SEPARATOR."$userid".DIRECTORY_SEPARATOR."MAX", 0777, true);   
    }   
    
    
    $photos_result=array();
    
    $photo=new telifoun\mqttchat\photo();    
    $total= count($_FILES['file']['name']);
    
       
    for( $i=0 ; $i < $total ; $i++ ) {
        
      if(is_array($_FILES['file']['tmp_name'])){
      $tempFile = $_FILES['file']['tmp_name'][$i];      
      $fileName = $_FILES['file']['name'][$i];       
      }else{
      $tempFile = $_FILES['file']['tmp_name'];      
      $fileName = $_FILES['file']['name'];   
      }
     
      /** max photo local disk path **/
      $maxFile = $targetPath.DIRECTORY_SEPARATOR.$userid.DIRECTORY_SEPARATOR."MAX".DIRECTORY_SEPARATOR.$userid.$fileName; 
      
      /** save photo in local path **/ 
      $move_result=move_uploaded_file($tempFile,$maxFile );
      if(!$move_result){                  
        $photos_result[$fileName]["ok"]=false;   
        $photos_result[$fileName]["error"]="Not uploaded because of error #".$_FILES["file"]["error"];   
                      
      }else{
      
          /** create photo thanbnail **/
          $photo->Thumbnail($maxFile,$targetPath.DIRECTORY_SEPARATOR.$userid.DIRECTORY_SEPARATOR."MIN",THUMBNAIL_WIDTH);

          /** min photo local path **/    
          $minFile= $targetPath.DIRECTORY_SEPARATOR.$userid.DIRECTORY_SEPARATOR."MIN".DIRECTORY_SEPARATOR.$userid.$fileName; 

          $pathinfo = pathinfo($minFile);
          $filename = $pathinfo['filename'];
          $ext = $pathinfo['extension'];
          
          /** build photo min and max URLs **/
          $minUrl=BASE_URL.'/photos/'.$userid."/MIN/".$userid.$fileName; 
          $maxUrl=BASE_URL.'/photos/'.$userid."/MAX/".$userid.$fileName; 

          $r=mqttchat_photo_upload($userid,
                                   $filename,
                                   $ext,
                                   $_FILES['file']['size'][$i],
                                   $type,
                                   $minFile,
                                   $maxFile,
                                   $minUrl,
                                   $maxUrl);  


          if($r["ok"]){
           $photos_result[$fileName]["ok"]=true;
           $photos_result[$fileName]["data"]["url_min"] =$minUrl;
           $photos_result[$fileName]["data"]["url_max"] =$maxUrl;
           
          }else{
           $photos_result[$fileName]["ok"]=false;  
           $photos_result[$fileName]["error"]=$r["error"];  
          }
      }
      
      
    } //end for
    
    $return=ok($photos_result);
    
 }catch(\Exception $ex){  
   $return=ko($ex->getCode(),$ex->getMessage());  
 }

}


header('Content-type: application/json; charset=utf-8');
echo json_encode($return);
exit;
   