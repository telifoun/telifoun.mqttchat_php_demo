<?php

/* 
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * 
 * This is a paid script developed by Gaddour Mohamed. It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */


echo <<<EOD
<div id="mqttchat_pictures_disk_for_\${contactid}" class="mqttchat-multiple-upload" class="dropzone">    
    <div class="dz-message needsclick">
        <div id="mqttchat-upload-text">
        <div class="mqttchat_upload_icon"></div>
        <span class="">\${dictDefaultMessage}</span>
        </div>
    </div>
    <div class="mqttchat_imagesPreview">        
        <ul id="mqttchat_images_list_\${contactid}" class="mqttchat_images_list"> 
        </ul>  
    </div>
</div>
<script id="mqttchat-file-upload-tpl" type="text/x-jquery-tmpl"> 
<li>
<div class="dz-preview dz-file-preview">
  <div class="dz-details">    
    <img data-dz-thumbnail />
    <button class="mqttchat-remove-img" title="Remove attachment" label="Remove attachment" data-dz-remove></button>
  </div> 
  <div class="dz-error-message"><span data-dz-errormessage></span></div>
</div>
</li>
</script>
EOD;

