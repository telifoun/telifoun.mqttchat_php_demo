<?php

/* 
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * 
 * This is a paid script developed by Gaddour Mohamed. It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */


/**
 * Indicates if you can send multiple photos in parallel
 */
define("UPLOAD_MULTIPLE",1);
/**
 * Specifies the maximum number of photos that can be dragged into drag zone
 */
define("MAX_PHOTOS_TO_DROP",5);
/**
 * Indicates the maximum size allowed for photo in MB
 */
define("MAX_PHOTO_SIZE",1); //MB


