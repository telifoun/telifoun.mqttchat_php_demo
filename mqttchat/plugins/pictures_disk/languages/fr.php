<?php

/* 
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * 
 * This is a paid script developed by Gaddour Mohamed. It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */


$rtl = '0';
$pictures_disk_language[0]="Cliquez ici pour charger des photos.";
$pictures_disk_language[1]="Votre navigateur ne supporte pas les téléchargements de fichiers drag'n'drop.";
$pictures_disk_language[2]="S'il vous plaît utiliser le formulaire ci-dessous pour télécharger vos fichiers comme dans les anciens jours.";
$pictures_disk_language[3]="Le fichier est trop gros ({{filesize}} MiB). Taille maximale des fichiers: {{maxFilesize}} MiB.";
$pictures_disk_language[4]="Vous ne pouvez pas télécharger des fichiers de ce typ";
$pictures_disk_language[5]="Le serveur a répondu avec le code {{statusCode}}.";
$pictures_disk_language[6]="Annuler le téléchargement";
$pictures_disk_language[7]="Téléchargement annulé.";
$pictures_disk_language[8]="Êtes-vous sûr de vouloir annuler ce téléchargement?";
$pictures_disk_language[9]="Effacer le fichier";
$pictures_disk_language[10]="Vous ne pouvez plus télécharger de fichiers.";
$pictures_disk_language[11]="Envoyé une photo enrigistrée.";
$pictures_disk_language[12]="Envoyer une photo enrigistrée.";
