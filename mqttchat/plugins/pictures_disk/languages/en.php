<?php

/* 
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * 
 * This is a paid script developed by Gaddour Mohamed. It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

$rtl = '0';
$pictures_disk_language[0]="Click here to upload photos.";
$pictures_disk_language[1]="Your browser does not support drag'n'drop file uploads.";
$pictures_disk_language[2]="Please use the fallback form below to upload your files like in the olden days.";
$pictures_disk_language[3]="File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.";
$pictures_disk_language[4]="You can't upload files of this type.";
$pictures_disk_language[5]="Server responded with {{statusCode}} code.";
$pictures_disk_language[6]="Cancel upload";
$pictures_disk_language[7]="Upload canceled.";
$pictures_disk_language[8]="Are you sure you want to cancel this upload?";
$pictures_disk_language[9]="Remove file";
$pictures_disk_language[10]="You can not upload any more files.";
$pictures_disk_language[11]="Sent a disk photo.";
$pictures_disk_language[12]="Send a disk photo.";
