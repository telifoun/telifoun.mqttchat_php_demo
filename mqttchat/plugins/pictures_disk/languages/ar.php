<?php

/* 
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * 
 * This is a paid script developed by Gaddour Mohamed. It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */


$rtl = '1';
$pictures_disk_language[0]="أنقر هنا لتحميل الصور";
$pictures_disk_language[1]="المتصفح الذي تستغمله لا يحتوي على خدمة إلصاق الصور مباشرة";
$pictures_disk_language[2]="Please use the fallback form below to upload your files like in the olden days.";
$pictures_disk_language[3]="الصورة حجمها كبير  ({{filesize}}MiB).الحجم الأقصى: {{maxFilesize}}MiB.";
$pictures_disk_language[4]="لا  يمكنك تحميل صور من هذا النوع.";
$pictures_disk_language[5]="الخادم أجاب  {{statusCode}} رقم.";
$pictures_disk_language[6]="ألغي التحميل";
$pictures_disk_language[7]="لقد ألغي التحميل";
$pictures_disk_language[8]="هل أنت متأكد أنك تريد إلغاء التحميل؟";
$pictures_disk_language[9]="فسخ الصورة";
$pictures_disk_language[10]="لا يمكنك تحميل صور أخرى في رسالة واحدة.";
$pictures_disk_language[11]="أرسل صورة مسجلة.";
$pictures_disk_language[12]="أرسل صورة مسجلة.";
