<?php

/* 
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * 
 * This is a paid script developed by Gaddour Mohamed. It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */


echo <<<EOD
<div class="mqttchat-player-container {{if (is_sent==true) }} record-sent {{else}} record-receive {{/if}}"> 
    <div class="mqttchat-play-progress" style="width:\${progress}px;">  
        <span class="mqttchat-button play" id="mqttchat-player-play" onClick="event.recordid='\${id}';event.contactid='\${contactid}';telifounJQ.records.play_record(event);" title="" {{if ((is_playing==true) || (is_loading==true )) }} style="display:none;" {{/if}}></span>
        <span class="mqttchat-button loading" title="" {{if (is_loading==false) }} style="display:none;" {{/if}}></span>
        <span class="mqttchat-button pause" id="mqttchat-player-pause" onClick="event.recordid='\${id}';event.contactid='\${contactid}';telifounJQ.records.pause_record(event)" title="" {{if ( (is_playing==false) || (is_idle==true)) }} style="display:none;" {{/if}}></span> 
        <span class="mqttchat-button error" id="mqttchat-player-error"  title="" {{if (is_error==false) }} style="display:none;" {{/if}}></span>  
        <hr>
        <span class="mqttchat-player-duration">\${duration}</span>
    </div>
</div>
EOD;
