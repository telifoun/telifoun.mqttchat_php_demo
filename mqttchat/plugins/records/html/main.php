<?php

/* 
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * 
 * This is a paid script developed by Gaddour Mohamed. It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */


echo <<<EOD
<div id="mqttchat_records_for_\${contactid}" class="mqttchat-record-container">

<div id="mqttchat-recorder">
  <div id="mqttchat-record-recordTime"><span id="mqttchat_rec_sw_m">00</span><span>:</span><span id="mqttchat_rec_sw_s">00</span></div>
  <div class="mqttchat-records-record">
   <div id="mqttchat-records-btn" class="mqttchat-records-ic-record"></div>
  </div>
</div>

<div id="mqttchat-recorder-preview">
   <div id="mqttchat-record-play"></div>
 </div>

<div id="mqttchat-record-processing">
    <div class="mqttchat-audio-processing"></div>    
    \${l_2}
</div>

<div id="mqttchat-record-cancel" class="mqttchat-record-cancelcss"></div>
</div>
EOD;
