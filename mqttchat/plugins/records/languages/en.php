<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

$rtl = '0';
$records_language[0]="Cancel";
$records_language[1]="Processing ...";
$records_language[2]="Sent Audio record";
$records_language[3]="Send Audio record";