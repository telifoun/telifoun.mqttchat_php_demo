/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function($){ 

$.visio_msg = $.visio_msg || (function(){   
    
 
  return {      
    /**
     * 
     * @returns {undefined}
     */
    startup : function(){ // louch in mqttchat startup
    
    },
    /**
     * 
     * @returns {undefined}
     */    
    init : function (){   //louch when init plugin     
      
    },
    /**
     * 
     * @returns {undefined}
     */
    send:function(touserid,action,duration,ok){ //on send message
        var type=$.MQTTchat.getPluginIndex(this.config.NAME);
        if(type>=0){
        $.MQTTchat.sendMessageTo(touserid,"",type,action,duration,ok);    
        }
    },
    /**
     * 
     * @param {type} message
     * @returns {undefined}
     */ 
    render:function(message){    //louch when render message   
       var send_date=message.send_date!==null?message.send_date:Math.round((new Date()).getTime() / 1000);
       return '<div class="mqttchat-visio-message">\n\
               <div class="content">\n\
                     <div class="msg">'+build_message(message)+'</div>\n\
                     <div class="date">'+local_Date(send_date)+ " "+local_Time(send_date)+((parseInt(message.p3)===1)?' - '+ message.p2 :'')+'</div>\n\
               </div>\n\
               <div class="cmd"><span onClick="telifounJQ.visio.recall_'+message.p1+'('+((message.from===$.MQTTchat.userid)?message.to:message.from)+')"><?=$visio_language[18]?></span></div>\n\
              </div>';
    },
    /**
     * 
     * @param {type} message
     * @returns {undefined}
     */ 
    preview:function(message){    // lounch when preview last message contact    
     return build_message(message);
    },
   
    /**
     * 
     */    
    config :{ // gui config plugin
     NAME:'visio_plugin',
     TYPE:'CUSTOM',
     USE_ICON:2,
     PLUGIN_LABEL:"APPEL VISIO"
    }
      
  };
  
    /**
     * 
     * @param {type} message
     * @returns {String}
     */
    function build_message(message){
      var msg='';
       if(parseInt(message.p3)===1){
           if(message.p1==="audio"){
             msg="<?=$visio_language[16]?>"  ;
           }else{
            msg="<?=$visio_language[17]?>"  ;
           }
       }else{
           if(message.from===$.MQTTchat.userid){ 
             msg="<?=$visio_language[14]?>";
           }else{
            msg='<span class="missed"><?=$visio_language[15]?></span>';   
           }
       }
    return msg;
    }
  
})();    
})(telifounJQ);