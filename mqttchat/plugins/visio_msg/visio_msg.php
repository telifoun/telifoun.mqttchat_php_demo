<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once dirname(__FILE__) .DIRECTORY_SEPARATOR.'config.php';

include_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'languages'.DIRECTORY_SEPARATOR.'en.php');
if (file_exists(dirname(__FILE__).DIRECTORY_SEPARATOR.'languages'.DIRECTORY_SEPARATOR.$lang.'.php')) {
include_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'languages'.DIRECTORY_SEPARATOR.$lang.'.php');
}

ob_start();

include_once dirname(__FILE__). '/visio_msg.js';


$js = ob_get_clean();
header('Content-type: text/javascript;charset=utf-8');
echo $js;