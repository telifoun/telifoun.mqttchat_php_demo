<?php

/* 
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * 
 * This is a paid script developed by Gaddour Mohamed. It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */


$rtl = '0';
$pictures_cam_language[0]="Image de webcam";
$pictures_cam_language[1]="Prendre une photo";
$pictures_cam_language[2]="Supprimer la photo";
$pictures_cam_language[3]="Envoyé image capturée.";
$pictures_cam_language[4]="Êtes-vous sûr de vouloir envoyer la photo capturée par webcam?";
