<?php

/* 
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * 
 * This is a paid script developed by Gaddour Mohamed. It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */


$rtl = '1';
$pictures_cam_language[0]="صورة فورية من الواب كام";
$pictures_cam_language[1]="إلتقط صورة فورية";
$pictures_cam_language[2]="فسخ الصورة";
$pictures_cam_language[3]="أرسل صورة  فورية من الواب كام";
$pictures_cam_language[4]="هل أنت متأكد من كونك تريد إرسال الصورة الملتقطة بصفة فورية من الواب كام ؟";
