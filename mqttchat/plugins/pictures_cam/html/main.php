<?php
include_once dirname(__FILE__) ."\..\config.php";
echo <<<HTML
<div class="mqttchat-plugin-cam-toolbar">
<ul class="mqttchat-toolbar-menu">
<li class="mqttchat-toolbar-item"><div id="mqttchat-plugin-cam-capture-btn" title="\${pictures_cam_language_1}"></div></li> 
<li class="mqttchat-toolbar-item"><div id="mqttchat-plugin-cam-capture-poubelle" title="\${pictures_cam_language_2}"></div></li>  
<li class="mqttchat-toolbar-item"><div id="mqttchat-plugin-cam-capture-send" title="\${pictures_cam_language_3}"></div></li>    
</ul>    
</div>
<div id="mqttchat_pictures_cam_for_\${contactid}" class="mqttchat-plugin-cam-content">
<video id="mqttchat-plugin-cam-video-\${contactid}" class="mqttchat-plugin-cam-video"></video>
<div class="mqttchat-plugin-cam-capture">
<canvas id="mqttchat-plugin-cam-canvas-\${contactid}" class="mqttchat-plugin-cam-canvas"></canvas>
<div id="mqttchat_upload_cam" class="dropzone"></div>
</div>
</div>
<div class="mqttchat-w3-light-grey">
<div class="mqttchat-w3-blue" style="height:24px;"></div>
</div>
HTML;
