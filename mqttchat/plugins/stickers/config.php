<?php

/* 
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * 
 * This is a paid script developed by Gaddour Mohamed. It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$meep = array (    
'meep_1'=>	'meep_1.png'  ,
'meep_2'=>	'meep_2.png'  ,
'meep_3'=>	'meep_3.png'  ,
'meep_4'=>	'meep_4.png'  ,
'meep_5'=>	'meep_5.png'  ,
'meep_6'=>	'meep_6.png'  ,
'meep_7'=>	'meep_7.png'  ,
'meep_8'=>	'meep_8.png'  ,
'meep_9'=>	'meep_9.png'  ,
'meep_10'=>	'meep_10.png'  ,
'meep_11'=>	'meep_11.png'  ,
'meep_12'=>	'meep_12.png'  ,
'meep_13'=>	'meep_13.png'  ,
'meep_14'=>	'meep_14.png'  ,
'meep_15'=>	'meep_15.png'  ,
'meep_16'=>	'meep_16.png'  ,
'meep_17'=>	'meep_17.png'  ,
'meep_18'=>	'meep_18.png'  ,
'meep_19'=>	'meep_19.png'  ,
'meep_20'=>	'meep_20.png'    
    
);


$droemon = array (    
'droemon_1'=>	'droemon_1.png'  ,
'droemon_2'=>	'droemon_2.png'  ,
'droemon_3'=>	'droemon_3.png'  ,
'droemon_4'=>	'droemon_4.png'  ,
'droemon_5'=>	'droemon_5.png'  ,
'droemon_6'=>	'droemon_6.png'  ,
'droemon_7'=>	'droemon_7.png'  ,
'droemon_8'=>	'droemon_8.png'  ,
'droemon_9'=>	'droemon_9.png'  ,
'droemon_10'=>	'droemon_10.png'  ,
'droemon_11'=>	'droemon_11.png'  ,
'droemon_12'=>	'droemon_12.png'  ,
'droemon_13'=>	'droemon_13.png'  ,
'droemon_14'=>	'droemon_14.png'  ,
'droemon_15'=>	'droemon_15.png'  ,
'droemon_16'=>	'droemon_16.png'  ,
'droemon_17'=>	'droemon_17.png'  ,
'droemon_18'=>	'droemon_18.png'  ,
'droemon_19'=>	'droemon_19.png'  ,
'droemon_20'=>	'droemon_20.png'  ,
'droemon_21'=>	'droemon_21.png'  ,
'droemon_22'=>	'droemon_22.png'  ,
'droemon_23'=>	'droemon_23.png'  ,
'droemon_24'=>	'droemon_24.png'  ,
'droemon_25'=>	'droemon_25.png'  ,
'droemon_26'=>	'droemon_26.png'  ,
'droemon_27'=>	'droemon_27.png'  ,
'droemon_28'=>	'droemon_28.png'  ,
    
);


$marie =array( 
'marie_1'=>	'marie_1.png'  ,
'marie_2'=>	'marie_2.png'  ,
'marie_3'=>	'marie_3.png'  ,
'marie_4'=>	'marie_4.png'  ,
'marie_5'=>	'marie_5.png'  ,
'marie_6'=>	'marie_6.png'  ,
'marie_7'=>	'marie_7.png'  ,
'marie_8'=>	'marie_8.png'  ,
'marie_9'=>	'marie_9.png'  ,
'marie_10'=>	'marie_10.png'  ,
'marie_11'=>	'marie_11.png'  ,
'marie_12'=>	'marie_12.png'  ,
'marie_13'=>	'marie_13.png'  ,
'marie_14'=>	'marie_14.png'  ,
'marie_15'=>	'marie_15.png'  ,
'marie_16'=>	'marie_16.png'  
);


$lovebigli =array( 
'lovebigli_1'=>	'lovebigli_1.png'  ,
'lovebigli_2'=>	'lovebigli_2.png'  ,
'lovebigli_3'=>	'lovebigli_3.png'  ,
'lovebigli_4'=>	'lovebigli_4.png'  ,
'lovebigli_5'=>	'lovebigli_5.png'  ,
'lovebigli_6'=>	'lovebigli_6.png'  ,
'lovebigli_7'=>	'lovebigli_7.png'  ,
'lovebigli_8'=>	'lovebigli_8.png'  ,
'lovebigli_9'=>	'lovebigli_9.png'  ,
'lovebigli_10'=>'lovebigli_10.png'  ,
'lovebigli_11'=>'lovebigli_11.png'  ,
'lovebigli_12'=>'lovebigli_12.png'  ,
'lovebigli_13'=>'lovebigli_13.png'  ,
'lovebigli_14'=>'lovebigli_14.png'  
);


$tickers = array_merge($meep,$droemon,$marie,$lovebigli);