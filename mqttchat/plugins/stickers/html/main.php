<?php

/* 
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * 
 * This is a paid script developed by Gaddour Mohamed. It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once dirname(__FILE__) ."/../config.php";

$meep_stickers  ="";
foreach($meep as $key => $value){
$class = str_replace("-"," ",preg_replace("/\.(.*)/","",$value));
$meep_stickers.='<span class="mqttchat_sticker_image '.$class.'" title="" onclick="telifounJQ.stickers.send(\''.$key.'\',${contactid});"></span>';
}

$droemon_stickers  ="";
foreach($droemon as $key => $value){
$class = str_replace("-"," ",preg_replace("/\.(.*)/","",$value));
$droemon_stickers.='<span class="mqttchat_sticker_image '.$class.'" title="" onclick="telifounJQ.stickers.send(\''.$key.'\',${contactid});"></span>';
}

$marie_stickers  ="";
foreach($marie as $key => $value){
$class = str_replace("-"," ",preg_replace("/\.(.*)/","",$value));
$marie_stickers.='<span class="mqttchat_sticker_image '.$class.'" title="" onclick="telifounJQ.stickers.send(\''.$key.'\',${contactid});"></span>';
}

$lovebigli_stickers  ="";
foreach($lovebigli as $key => $value){
$class = str_replace("-"," ",preg_replace("/\.(.*)/","",$value));
$lovebigli_stickers.='<span class="mqttchat_sticker_image '.$class.'" title="" onclick="telifounJQ.stickers.send(\''.$key.'\',${contactid});"></span>';
}

echo <<<EOD

<ul class='mqttchat_stickers_tabs'>
  <li id="meep" class="active"></li>
  <li id="droemon"></li>
  <li id="marie"></li>
  <li id="lovebigli"></li>
</ul>
  
<div id="mqttchat_stickers_for_\${contactid}" class="mqttchat_stickers_container">

<div id="meep_content" class="mqttchat_sticker_content">
{$meep_stickers}
</div>

<div id="droemon_content" class="mqttchat_sticker_content">
{$droemon_stickers}
</div>

<div id="marie_content" class="mqttchat_sticker_content">
{$marie_stickers}
</div>

<div id="lovebigli_content" class="mqttchat_sticker_content">
{$lovebigli_stickers}
</div>

</div>
EOD;
