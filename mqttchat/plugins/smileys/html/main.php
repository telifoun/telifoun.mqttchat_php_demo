<?php

/* 
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * 
 * This is a paid script developed by Gaddour Mohamed. It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */


include_once dirname(__FILE__) ."/../config.php";
$people_text = '';
foreach($people as $key => $value){
$class = str_replace("-"," ",preg_replace("/\.(.*)/","",$value));
$people_text.='<span class="mqttchat_smiley mq_people_'.$class.'" pattern="'.$key.'" title="" onclick="telifounJQ.smileys.addtext(\''.$key.'\',${contactid});"></span>';
}

$car_text = '';
foreach($car as $key => $value){
 $class = str_replace("-"," ",preg_replace("/\.(.*)/","",$value));
 $car_text.='<span class="mqttchat_smiley mq_car_'.$class.'" title="" onclick="telifounJQ.smileys.addtext(\''.$key.'\',${contactid});"></span>';
}

$nature_text = '';
foreach($nature as $key => $value){
 $class = str_replace("-"," ",preg_replace("/\.(.*)/","",$value));
 $nature_text.='<span class="mqttchat_smiley mq_nature_'.$class.'" title="" onclick="telifounJQ.smileys.addtext(\''.$key.'\',${contactid});"></span>';
}

$sport_text = '';
foreach($sport as $key => $value){
$class = str_replace("-"," ",preg_replace("/\.(.*)/","",$value));
$sport_text.='<span class="mqttchat_smiley mq_sport_'.$class.'" title="" onclick="telifounJQ.smileys.addtext(\''.$key.'\',${contactid});"></span>';
}

$food_text = '';
foreach($food as $key => $value){
$class = str_replace("-"," ",preg_replace("/\.(.*)/","",$value));
$food_text.='<span class="mqttchat_smiley mq_food_'.$class.'" title="" onclick="telifounJQ.smileys.addtext(\''.$key.'\',${contactid});"></span>'; 
}

$electronic_text = '';
foreach($electronic as $key => $value){
$class = str_replace("-"," ",preg_replace("/\.(.*)/","",$value));
$electronic_text.='<span class="mqttchat_smiley mq_electronic_'.$class.'" title="" onclick="telifounJQ.smileys.addtext(\''.$key.'\',${contactid});"></span>'; 
}

$symbols_text = '';
foreach($symbols as $key => $value){
$class = str_replace("-"," ",preg_replace("/\.(.*)/","",$value));
$symbols_text.='<span class="mqttchat_smiley mq_symbols_'.$class.'" title="" onclick="telifounJQ.smileys.addtext(\''.$key.'\',${contactid});"></span>'; 
}

echo <<<EOD

<ul class='mqttchat_smileys_tabs'>
  <li id="people" class="active"></li>
  <li id="car"></li>
  <li id="nature"></li>
  <li id="sport"></li>
  <li id="food"></li>
  <li id="electronic"></li>
  <li id="symbols"></li>
</ul>
  
<div id="mqttchat_smileys_for_\${contactid}" class="mqttchat_smileys_container">

<div id="people_content" class="mqttchat_smiley_content">
{$people_text}
</div>
<div id="car_content" class="mqttchat_smiley_content">
{$car_text} 
</div>
<div id="nature_content" class="mqttchat_smiley_content">
{$nature_text}
</div>  
<div id="sport_content" class="mqttchat_smiley_content">
{$sport_text}
</div>
<div id="food_content" class="mqttchat_smiley_content">
{$food_text}
</div>
<div id="electronic_content" class="mqttchat_smiley_content">
{$electronic_text }
</div>  
<div id="symbols_content" class="mqttchat_smiley_content">
{$symbols_text}
</div>
  
</div>
EOD;
