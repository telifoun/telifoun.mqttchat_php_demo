<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */

include_once dirname(__FILE__) .DIRECTORY_SEPARATOR.'config.php';
include_once(dirname(__FILE__) .DIRECTORY_SEPARATOR.'shared_f.php');
include_once dirname(__FILE__) .DIRECTORY_SEPARATOR.'mqttchat.php';
include_once dirname(__FILE__) .DIRECTORY_SEPARATOR.'jsmin.php'; 

$mtime = explode(" ",microtime());
$starttime = $mtime[1]+$mtime[0];
$HTTP_USER_AGENT = '';
$useragent = (isset($_SERVER["HTTP_USER_AGENT"])) ? $_SERVER["HTTP_USER_AGENT"] : $HTTP_USER_AGENT; 
ob_start();

if(file_exists(dirname(__FILE__) .DIRECTORY_SEPARATOR."cache".DIRECTORY_SEPARATOR."mqttchat.$lang.js") && (DEV_MODE!=1)){
   if(!empty($_SERVER['HTTP_IF_MODIFIED_SINCE'])&&strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE'])==filemtime(dirname(__FILE__).DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR."mqttchat.$lang.js")){
	   header("HTTP/1.1 304 Not Modified");
	   exit();
	}
  	readfile(dirname(__FILE__).DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR."mqttchat.$lang.js");
	$js = ob_get_clean();
} else{

  if(INCLUDE_JQUERY=='1'){
  include_once dirname(__FILE__). '/js/jquery.min.js';
  }

  if(INCLUDE_JQUERY_TMPL=='1'){
  include_once dirname(__FILE__). '/js/jquery.tmpl.min.js';
  }


  include_once dirname(__FILE__). '/js/js-import.min.js';

 
  if(isset($modules) && sizeof($modules)>0){
    foreach($modules as $module){  
     include_once dirname(__FILE__). '/modules/'.$module.'/'.$module.'.php';
    }
  }


 include_once dirname(__FILE__). '/js/js.min.js';
 
 if(phpversion()>='5'){
	$js = JSMin::minify(ob_get_clean());
 }else{
	$js = ob_get_clean();
 }    

 $fp = @fopen(dirname(__FILE__).DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR."mqttchat.$lang.js",'w');
 @fwrite($fp,$js);
 @fclose($fp);
 
}

$lastModified = filemtime(dirname(__FILE__).DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR."mqttchat.$lang.js");

header('Content-type: text/javascript;charset=utf-8');
header("Last-Modified: ".gmdate("D, d M Y H:i:s",$lastModified)." GMT");
header('Expires: '.gmdate("D, d M Y H:i:s",time()+3600*24*365).' GMT');
echo $js;

$mtime = explode(" ",microtime());
$endtime = $mtime[1]+$mtime[0];

echo "\n\n/* Execution time: ".($endtime-$starttime)." seconds */";
function cleanInput($input){
 return strtolower(preg_replace("/[^+A-Za-z0-9\_]/","",trim($input)));
}   