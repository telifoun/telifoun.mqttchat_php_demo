<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */
 
include_once dirname(__FILE__) .DIRECTORY_SEPARATOR.'config.php';
include_once(dirname(__FILE__) .DIRECTORY_SEPARATOR.'shared_f.php');
include_once dirname(__FILE__) .DIRECTORY_SEPARATOR.'mqttchat.php';
include_once dirname(__FILE__) .DIRECTORY_SEPARATOR.'jsmin.php'; 

define ("TEMPLATES_URL",BASE_URL."/core/templates/docked/html");

$mtime = explode(" ",microtime());
$starttime = $mtime[1]+$mtime[0];
$HTTP_USER_AGENT = '';
$useragent = (isset($_SERVER["HTTP_USER_AGENT"])) ? $_SERVER["HTTP_USER_AGENT"] : $HTTP_USER_AGENT; 
ob_start();

if(file_exists(dirname(__FILE__) .DIRECTORY_SEPARATOR."cache".DIRECTORY_SEPARATOR."mqttchat-docked.$lang.js") && (DEV_MODE!=1)){
   if(!empty($_SERVER['HTTP_IF_MODIFIED_SINCE'])&&strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE'])==filemtime(dirname(__FILE__).DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR."mqttchat-docked.$lang.js")){
	   header("HTTP/1.1 304 Not Modified");
	  exit();
	}
  	readfile(dirname(__FILE__).DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR."mqttchat-docked.$lang.js");
	$js = ob_get_clean();
} else{

  include_once dirname(__FILE__). '/js/js-docked-import.min.js';


  if(isset($plugins) && sizeof($plugins)>0){
    foreach($plugins as $plugin){  
    include_once dirname(__FILE__). '/plugins/'.$plugin.'/'.$plugin.'.php';
    }
  }     

  include_once dirname(__FILE__). '/js/js-docked.min.js';

    
  if(phpversion()>='5'){
	 $js = JSMin::minify(ob_get_clean());
  }else{
 	$js = ob_get_clean();
  }  

 $fp = @fopen(dirname(__FILE__).DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR."mqttchat-docked.$lang.js",'w');
 @fwrite($fp,$js);
 @fclose($fp);
 
}

$lastModified = filemtime(dirname(__FILE__).DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR."mqttchat-docked.$lang.js");

header('Content-type: text/javascript;charset=utf-8');
header("Last-Modified: ".gmdate("D, d M Y H:i:s",$lastModified)." GMT");
header('Expires: '.gmdate("D, d M Y H:i:s",time()+3600*24*365).' GMT');
echo $js;

$mtime = explode(" ",microtime());
$endtime = $mtime[1]+$mtime[0];

echo "\n\n/* Execution time: ".($endtime-$starttime)." seconds */";
function cleanInput($input){
 return strtolower(preg_replace("/[^+A-Za-z0-9\_]/","",trim($input)));
}   