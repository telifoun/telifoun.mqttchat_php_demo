<?php

/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */


/* LANGUAGE */
$rtl = '1';
$language[0]="MQTTCHAT";
$language[1]="الإعدادات";
$language[2]="تغيير حالة";
$language[3]="متاح";
$language[4]="مشغول";
$language[5]="المستخدم غير متصل بالموقع .الرجاء أولا الإتصال بخادم البرمجة.";
$language[6]="Websockets are not supported on this Browser! <br> try to upgrade browser to newer version.";
$language[7]="أنت :";
$language[8]="أرسل ملصق.";
$language[9]="بصدد الكتابة ...";
$language[10]="Websocket Support !";
$language[11]="قرأت الرسالة";
$language[12]="Online there is";
$language[13]="ساعة";
$language[14]="يوم";
$language[15]="شهر";
$language[16]="سنة";
$language[17]="إعلامات بإستعمال الصوت";
$language[18]="إتصال بالصوت";
$language[19]="إتصال بالفيديو";
$language[20]="أفتح محادثة";
$language[21]="حضر المستخدم";
$language[22]="حذف الحضر عن المستخدم";
$language[23]="فسخ المحادثة";
$language[24]="حذف المستخدم من قائمة الأصحاب";
$language[25]="هل أنت متأكد من كونك تريد حذف  ";
$language[26]="هل أنت متأكد من كونك تريد رفع الحضر عن  ";
$language[27]="البحث عن شخص";
$language[28]="هل انت متأكد من كونك تريد فسخ المحادثة ";
$language[29]="رجل";
$language[30]="إمرأة";
$language[31]="بحث";
$language[32]="تغيير المعلومات";
$language[33]="الخروج من الدردشة";
$language[34]="ملف فارغ ";
$language[35]="إجراء غير معروف!";
$language[36]="رسالة جديدة";
$language[37]="السيد";
$language[38]="السيدة";
$language[39]="الأنسة";
$language[40]="أنت";
$language[41]="اللقب";
$language[42]="الإسم";
$language[43]="تحميل الصورة من القرص";
$language[44]="تسجيل المعلومات";
$language[45]="Enable online status to activate MQTTCHAT.";
$language[46]="تفعيل";
$language[47]="Are you sure you want to disconnect from MQTTCHAT?";
$language[48]="إتصال صوتي";
$language[49]="إتصال بالفيديو";
$language[50]="أغلق المحادثة";
$language[51]="هل أنت متأكد من كوتك تريد حذف الشخص من قائمة أصدقائك?";
$language[60]="سيؤدي إغلاق مربع الدردشة إلى إغلاق جميع المكونات الإضافية النشطة. هل تريد حقا أن تغلق?";
$language[61]="تحيين الصفحة";
$language[62]="أرسل العطب ";
$language[63]="دقيقة";
$language[64]="أكتب رسالة ";
$language[65]="الإطلاع على الملف الشخصي";
$language[66]="انقر هنا لإرسال الرسالة";
$language[67]="دردشة حينية";
$language[68]="(مغلقة)";
$language[69]="Data field missing";
$language[70]="Wrong or missing mandatory user id data in mqttchat element";
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////