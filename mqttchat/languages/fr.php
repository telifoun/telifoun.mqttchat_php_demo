<?php


/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */


/* LANGUAGE */
$rtl = '0';
$language[0]="MQTTCHAT";
$language[1]="Paramétres";
$language[2]="MODIFIER PRESENCE";
$language[3]="En ligne";
$language[4]="Occupé";
$language[5]="Not connected user. Please connect to mqttchat server first.";
$language[6]='Websockets are not supported on this Browser! if you are using an outdated browser, you can upgrade it <a href=\"http://browsehappy.com\">here</a>.';
$language[7]="Vous: ";
$language[8]="Envoyé un sticker.";
$language[9]="En train d'écrire ...";
$language[10]="Websocket Support !";
$language[11]="Vu à";
$language[12]="En ligne il y a";
$language[13]="Heur(s)";
$language[14]="Jour(s)";
$language[15]="Mois";
$language[16]="Année(s)";
$language[17]="Notifications audio";
$language[18]="Appels Audios";
$language[19]="Appels Vidéos";
$language[20]="Ouvrir Conversation";
$language[21]="Bloquer Utilisateur";
$language[22]="Débloquer Utilisateur";
$language[23]="Supprimer Conversation";
$language[24]="Supprimer de la liste d'amis";
$language[25]="Êtes-vous sûr de vouloir bloquer ";
$language[26]="Êtes-vous sûr de vouloir débloquer ";
$language[27]="Rechercher une personne";
$language[28]="Êtes-vous sûr de vouloir supprimer la conversation avec ";
$language[29]="Homme";
$language[30]="Femme";
$language[31]="Filter";
$language[32]="Modifier profile";
$language[33]="Se déconnecter";
$language[34]="Fichier reçue vide!";
$language[35]="Action inconnue!";
$language[36]="nouveau message(s)";
$language[37]="Mr.";
$language[38]="Mme";
$language[39]="Melle";
$language[40]="VOUS ÊTES";
$language[41]="NOM";
$language[42]="PRENOM";
$language[43]="Télécharger une photo à partir de disque";
$language[44]="Enrégistrer les informations";
$language[45]="Enable online status to activate MQTTCHAT.";
$language[46]="Activer";
$language[47]="Vous etes sure que vous voulz vous déconnecter de MQTTCHAT?";
$language[48]="Appel audio";
$language[49]="Appel vidéo";
$language[50]="Fermer la fenetre";
$language[51]="Are you sure you want to delete user from friends list?";
$language[60]="Closing the chatbox will close all active plugins. Do you really want to close?";
$language[61]="Rafraîchir la page";
$language[62]="Envoyer l'erreur";
$language[63]="min";
$language[64]="Tapez votre message";
$language[65]="Consulter profile";
$language[66]="Cliquez ici pour envoyer le message";
$language[67]="tchats";
$language[68]="(Désactivé)";
$language[69]="Champ de données manquant";
$language[70]="Données d'identification d'utilisateur incorrectes ou manquantes dans l'élément mqttchat";
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////