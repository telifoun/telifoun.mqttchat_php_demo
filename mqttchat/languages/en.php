<?php


/*
 * Copyright (C) 2019 Gaddour, Gaddour Mohamed
 * This is a paid script developed by Gaddour Mohamed (m.gaddour@yahoo.fr). 
 * It is strictly forbidden to publish it for free or to sell it to a third party without the prior consent of the author.
 * Any misuse of product or income related to its exploitation is strictly prohibited.
 */


/* LANGUAGE */
$rtl = '0';
$language[0]="MQTTCHAT";
$language[1]="Settings";
$language[2]="Edit Status";
$language[3]="Online";
$language[4]="Busy";
$language[5]="Not connected user. Please connect to mqttchat server first.";
$language[6]="Websockets are not supported on this Browser! <br> try to upgrade browser to newer version.";
$language[7]="You: ";
$language[8]="Sent a sticker.";
$language[9]="Typing ...";
$language[10]="Websocket Support !";
$language[11]="Seen at";
$language[12]="Online there is";
$language[13]="Hour(s)";
$language[14]="Day(s)";
$language[15]="Month(s)";
$language[16]="Year(s)";
$language[17]="Sound Notifications";
$language[18]="Voice calls";
$language[19]="Video calls";
$language[20]="Open conversation";
$language[21]="Block user";
$language[22]="Unlock user";
$language[23]="Delete conversation";
$language[24]="Remove from friends list";
$language[25]="Are you sure you want to block ";
$language[26]="Are you sure you want to unlock ";
$language[27]="Search a person";
$language[28]="Are you sure you want to delete conversation with ";
$language[29]="Male";
$language[30]="Female";
$language[31]="Filter";
$language[32]="Edit Profile";
$language[33]="Disconnect";
$language[34]="Empty file!";
$language[35]="Unknown action!";
$language[36]="new message(s)";
$language[37]="Mr.";
$language[38]="Mrs";
$language[39]="Miss";
$language[40]="YOU ARE";
$language[41]="NAME";
$language[42]="SURNAME";
$language[43]="Upload photo fom disk";
$language[44]="Save informations";
$language[45]="Enable online status to activate MQTTCHAT.";
$language[46]="Activate";
$language[47]="Are you sure you want to disconnect from MQTTCHAT?";
$language[48]="Audio Call";
$language[49]="Video Call";
$language[50]="Close chat window";
$language[51]="Are you sure you want to delete user from friends list?";
$language[60]="Closing the chatbox will close all active plugins. Do you really want to close?";
$language[61]="Refresh";
$language[62]="Send Error Message";
$language[63]="min";
$language[64]="Type your message";
$language[65]="View profile";
$language[66]="Click here to send the message";
$language[67]="Chats";
$language[68]="(OFF)";
$language[69]="Data field missing";
$language[70]="Wrong or missing mandatory user id data in mqttchat element";

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////