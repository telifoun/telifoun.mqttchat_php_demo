<?php

/* 
 * Demo application - use of MQTTCHAT PHP SDK
 * for more information read PHP SDK documentation : https://mqttchat.telifoun.com/doc/sdk * 
 */

require_once 'mqttchat/sdk/vendor/autoload.php';

$user =new telifoun\mqttchat\user();
/*set user id*/
$user->_setUserid(1);
/* edit user information */
$user->_setName("whatever");

/*update user*/
$result = $user->Update();

var_dump($result);

