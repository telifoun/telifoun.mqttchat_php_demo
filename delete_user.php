<?php

/* 
 * Demo application - use of MQTTCHAT PHP SDK
 * for more information read PHP SDK documentation : https://mqttchat.telifoun.com/doc/sdk * 
 */


require_once 'mqttchat/sdk/vendor/autoload.php';

$user =new telifoun\mqttchat\user();

/** set mqttchat user id **/
$user->_setUserid(1);
/** delete specific user  from your mqttchat users list **/
$result=$user->Remove();

var_dump($result);