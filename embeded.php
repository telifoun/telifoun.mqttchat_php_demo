

<html>
    <head>     
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <script src="mqttchat/js.php"></script>
    <link href="mqttchat/css.php" media="screen" rel="stylesheet" type="text/css" />  
    <script src="mqttchat/js-embeded.php"></script>
    <link href="mqttchat/css-embeded.php" media="screen" rel="stylesheet" type="text/css" />
    <script>
        
    $(document).ready(function() { 
        
    telifounJQ(MQTTchat).on('mqttchat-load-complete', function (e) {    
    console.log("--->MQTTCHAT LOAD COMPLETE EVENT");
    //telifounJQ.MQTTchat.__initChatWithUser(233);
    });
        
    telifounJQ(MQTTchat).on('mqttchat-user-presence-update', function (e) {    
    console.log("--->MQTTCHAT INCOMING USER PRESENCE UPDATE EVENT : [userid :"+e.userid+",status:"+e.status+",audioCAP:"+e.audioCAP+",videoCAP:"+e.videoCAP+",timestamp:"+e.timestamp+"]");
    });
 
    telifounJQ(MQTTchat).on('mqttchat-send-message', function (e) {    
    console.log("---> MQTTCHAT SEND MESSAGE EVENT : [toUserid :"+e.toUserid+",message:"+e.message+"]");
    });
    
   
    telifounJQ(MQTTchat).on('mqttchat-ack-message', function (e) {    
    console.log("--->MQTTCHAT ACK MESSAGE EVENT: [ toUserid:"+e.toUserid+", ack:["+e.ack+"] , message: ["+e.message+"]]");
    });
    
    telifounJQ(MQTTchat).on('mqttchat-reject-message', function (e) {    
    console.log("--->MQTTCHAT REJECT MESSAGE EVENT: [toUserid:"+e.toUserid+", why: ["+e.why+"],message: ["+e.message+"]]");
    });
    
    telifounJQ(MQTTchat).on('mqttchat-incoming-message', function (e) {    
    console.log("--->MQTTCHAT INCOMING MESSAGE EVENT: [fromUserid: "+e.fromUserid+" ,message: ["+e.message+"]]");
    });
    
    telifounJQ(MQTTchat).on('mqttchat-typing-message', function (e) {    
    console.log("--->MQTTHCAT TYPING MESSAGE EVENT:[fromUserid:"+e.fromUserid+", on: "+e.on+"]");
    });
    
    telifounJQ(MQTTchat).on('mqttchat-reading-message', function (e) {    
    console.log("--->MQTTHCAT READING MESSAGE EVENT:[toUserid:"+e.toUserid+",messageId: "+e.messageId+",readDate:"+e.readDate+"]");
    });
    
    telifounJQ(MQTTchat).on('mqttchat-not-read-messages-count-update', function (e) {    
    console.log("--->MQTTHCAT NOT READ MESSAGES COUNT EVENT:[n:"+e.n+"]");
    });
    
    telifounJQ(MQTTchat).on('mqttchat-error', function (e) {    
    console.log("--->MQTTCHAT ERROR EVENT: ["+e.error+"]");
    });
    
    });
    </script>
    </head>
    <body>
    <div id="mqttchat" data-user-id="<?=$_GET["userid"]?>"></div>    
   
</body>
</html>