<?php

/* 
 * Demo application - use of MQTTCHAT PHP SDK
 * for more information read PHP SDK documentation : https://mqttchat.telifoun.com/doc/sdk * 
 */

require_once 'mqttchat/sdk/vendor/autoload.php';

$user =new telifoun\mqttchat\user();
/*define user basic informations with Set function*/
$user->Set( 1      , // user id
           "user", // user name
           "one",  //user surname         
           ""  , //user profile link
           ""  , //user avatar link
           telifoun\mqttchat\user::GENDER_MALE //user gender
        );

/*add him to your MQTTCHAT users list*/
$result = $user->Add();

if($result["ok"]){
  /**  
   * show docked or embeded chat 
   * **/
  $userid= $result["response"]["userid"]; 
  header('Location: embeded.php?userid='.$userid); 
  //header('Location: docked.php?userid='.$userid); 
}




